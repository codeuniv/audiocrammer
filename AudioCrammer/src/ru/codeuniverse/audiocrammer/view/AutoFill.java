package ru.codeuniverse.audiocrammer.view;

import java.io.Serializable;

public class AutoFill implements Serializable {

	String topicGroup_1;
	String topic_1;
	String subTopic_1;

	String topicGroup_2;
	String topic_2;
	String subTopic_2;

	String topicGroup_3;
	String topic_3;
	String subTopic_3;

	public void addTopicGroup(String topicGroup) {
		String oldTopicGroup_1 = topicGroup_1;
		String oldTopicGroup_2 = topicGroup_2;
		topicGroup_1 = topicGroup;
		topicGroup_2 = oldTopicGroup_1;
		topicGroup_3 = oldTopicGroup_2;
	}

	public void addTopic(String topic) {
		String oldTopic_1 = topic_1;
		String oldTopic_2 = topic_2;
		topic_1 = topic;
		topic_2 = oldTopic_1;
		topic_3 = oldTopic_2;
	}

	public void addSubTopic(String subTopic) {
		String oldSubTopic_1 = subTopic_1;
		String oldSubTopic_2 = subTopic_2;
		subTopic_1 = subTopic;
		subTopic_2 = oldSubTopic_1;
		subTopic_3 = oldSubTopic_2;
	}

	public String getTopicGroup_1() {
		return topicGroup_1;
	}

	public void setTopicGroup_1(String topicGroup_1) {
		this.topicGroup_1 = topicGroup_1;
	}

	public String getTopic_1() {
		return topic_1;
	}

	public void setTopic_1(String topic_1) {
		this.topic_1 = topic_1;
	}

	public String getSubTopic_1() {
		return subTopic_1;
	}

	public void setSubTopic_1(String subTopic_1) {
		this.subTopic_1 = subTopic_1;
	}

	public String getTopicGroup_2() {
		return topicGroup_2;
	}

	public void setTopicGroup_2(String topicGroup_2) {
		this.topicGroup_2 = topicGroup_2;
	}

	public String getTopic_2() {
		return topic_2;
	}

	public void setTopic_2(String topic_2) {
		this.topic_2 = topic_2;
	}

	public String getSubTopic_2() {
		return subTopic_2;
	}

	public void setSubTopic_2(String subTopic_2) {
		this.subTopic_2 = subTopic_2;
	}

	public String getTopicGroup_3() {
		return topicGroup_3;
	}

	public void setTopicGroup_3(String topicGroup_3) {
		this.topicGroup_3 = topicGroup_3;
	}

	public String getTopic_3() {
		return topic_3;
	}

	public void setTopic_3(String topic_3) {
		this.topic_3 = topic_3;
	}

	public String getSubTopic_3() {
		return subTopic_3;
	}

	public void setSubTopic_3(String subTopic_3) {
		this.subTopic_3 = subTopic_3;
	}

}
