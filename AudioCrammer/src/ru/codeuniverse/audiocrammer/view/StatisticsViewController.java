package ru.codeuniverse.audiocrammer.view;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.Window;
import ru.codeuniverse.audiocrammer.model.Backlog;
import ru.codeuniverse.audiocrammer.model.Statistics;

public class StatisticsViewController implements Initializable {

	private static final Logger logger = LoggerFactory.getLogger(MainViewController.class);

	@FXML
	private AnchorPane rootStatsAnchorPane;

	@FXML
	private Label totalMinLabel;

	@FXML
	private Label totalNumOfFilesLabel;

	@FXML
	private Label inProgressMinLabel;

	@FXML
	private Label inProgressNumOfFilesLabel;

	@FXML
	private Label notCheckedOutMinLabel;

	@FXML
	private Label notCheckedOutNumOfFilesLabel;

	@FXML
	private Label dueForReviewTodayMinLabel;

	@FXML
	private Label dueForReviewTodayNumOfFilesLabel;

	@FXML
	private Label dueForReviewOutstandingMinLabel;

	@FXML
	private Label dueForReviewOutstandingNumOfFilesLabel;

	@FXML
	private Label dueForReviewAllMinLabel;

	@FXML
	private Label dueForReviewAllNumOfFilesLabel;

	@FXML
	private Label inProgressLabel;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		logger.debug("Entered StatisticsView initialize() method");

		initToolTips();

		Statistics stats = null;
		Backlog backlog = null;
		try {
			backlog = new Backlog();
			stats = backlog.getStatistics();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// setting totalNumOfFilesLabel
		totalNumOfFilesLabel.setText(String.valueOf(stats.getCountAllFiles()));

		// setting totalMinLabel
		String totalMinText = String.valueOf(stats.getAllFilesDuration());
		totalMinLabel.setText(totalMinText);

		// setting notCheckedOutNumOfFilesLabel
		String notCheckedOutNumOfFilesText = String.valueOf(stats.getCountNotCheckedOut());
		notCheckedOutNumOfFilesLabel.setText(notCheckedOutNumOfFilesText);

		// setting notCheckedOutDuration
		String notCheckedOutDurationText = String.valueOf(stats.getNotCheckedOutDuration());
		notCheckedOutMinLabel.setText(notCheckedOutDurationText);

		// setting inProgressNumOfFilesLabel
		String inProgressNumOfFilesLabelText = String.valueOf(stats.getCountCheckedOut());
		inProgressNumOfFilesLabel.setText(inProgressNumOfFilesLabelText);

		// setting inProgressDuration
		String inProgressDurationLabelText = String.valueOf(stats.getCheckedOutDuration());
		inProgressMinLabel.setText(inProgressDurationLabelText);

		// setting dueForReviewAllNumOfFilesLabel
		String dueForReviewAllNumOfFilesLabelText = String
				.valueOf(stats.getCountScheduledNotReviewedAll());
		dueForReviewAllNumOfFilesLabel.setText(dueForReviewAllNumOfFilesLabelText);

		// dueForReviewTodayNumOfFilesLabel
		dueForReviewTodayNumOfFilesLabel
				.setText(String.valueOf(stats.getCountScheduledTodayNotReviewed()));

		// dueForReviewTodayMinLabel
		dueForReviewTodayMinLabel
				.setText(String.valueOf(stats.getScheduledTodayNotReviewedDuration()));

		// dueForReviewAllNumOfFilesLabel
		dueForReviewAllNumOfFilesLabel
				.setText(String.valueOf(stats.getCountScheduledNotReviewedAll()));

		// dueForReviewAllMinLabel
		dueForReviewAllMinLabel
				.setText(String.valueOf(stats.getScheduledNotReviewedAllDuration()));

		// dueForReviewOutstandingNumOfFilesLabel
		int outstandingNumOfFiles = stats.getCountScheduledNotReviewedAll()
				- stats.getCountScheduledTodayNotReviewed();
		dueForReviewOutstandingNumOfFilesLabel.setText(String.valueOf(outstandingNumOfFiles));

		// dueForReviewOutstandingMinLabel
		long outstandingMin = stats.getScheduledNotReviewedAllDuration()
				- stats.getScheduledTodayNotReviewedDuration();
		dueForReviewOutstandingMinLabel.setText(String.valueOf(outstandingMin));

		// on ESC close adjustRecord form and do nothing
		this.rootStatsAnchorPane.addEventHandler(KeyEvent.KEY_RELEASED, (KeyEvent event) -> {
			if (KeyCode.ESCAPE == event.getCode()) {
				Window wnd = ((Node) event.getSource()).getScene().getWindow();
				Stage thisStage = (Stage) wnd;
				thisStage.hide();
			}
		});
	}

	private void initToolTips() {
		Tooltip inProgressToolTip = new Tooltip();
		String inProgressToolTipText = "Checked-out (so studied at least once at check-out,"
				+ " or maybe even reviewed 1-etc times later)";
		inProgressLabel.setTooltip(inProgressToolTip);

	}

}
