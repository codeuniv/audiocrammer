package ru.codeuniverse.audiocrammer.view;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.prefs.BackingStoreException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.Window;
import ru.codeuniverse.audiocrammer.model.Settings;

public class MasterSettingsViewController implements Initializable {

	private static final Logger logger = LoggerFactory
			.getLogger(MasterSettingsViewController.class);

	@FXML
	private AnchorPane rootAnchorPane;

	@FXML
	private CheckBox changeInternalFolderEnabledCheckBox;

	@FXML
	private Button changeInternalFolderBtn;

	@FXML
	private Label currentInternalFolderLabel;

	@FXML
	private Label internalFolderLabel;

	@FXML
	void resetAllSettingsBtnHandler(ActionEvent event) {

		// add @Are you sure? window
		Settings settings = new Settings();
		try {
			settings.resetAllSettings();
			Platform.exit(); // EXIT APPLICATION !!!
		} catch (BackingStoreException e) {
			// TODO Auto-generated catch block
			logger.info("BackingStoreException when try resetAllSettings");
			e.printStackTrace();
			showErrorResettingsSettingsMsg();
		}

	}

	private void showErrorResettingsSettingsMsg() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("ERROR WHILE RESETTINGS SETTINGS!");
		alert.setHeaderText("Attempt to reset settings failed!");
		alert.setContentText("Attempt to reset all settings failed!");
		alert.showAndWait();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		Settings settings = new Settings();
		currentInternalFolderLabel.setText(settings.getInternalFolderParentDirectory());
		internalFolderLabel.setText(settings.getAppDir());

		addCloseFormOnESC();

		// BIND Changing Internal dir Btn enabled
		changeInternalFolderBtn.disableProperty()
				.bind(Bindings.not(changeInternalFolderEnabledCheckBox.selectedProperty()));
	}

	private void addCloseFormOnESC() {
		// on ESC close adjustRecord form and do nothing
		rootAnchorPane.addEventHandler(KeyEvent.KEY_RELEASED, (KeyEvent event) -> {
			if (KeyCode.ESCAPE == event.getCode()) {
				Window wnd = ((Node) event.getSource()).getScene().getWindow();
				Stage settingsStage = (Stage) wnd;
				settingsStage.hide();
			}
		});
	}

	@FXML
	void changeInternalFolderBtnAction(ActionEvent event) {

	}

}
