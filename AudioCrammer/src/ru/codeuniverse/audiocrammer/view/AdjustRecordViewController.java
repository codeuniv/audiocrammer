package ru.codeuniverse.audiocrammer.view;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.math3.util.Precision;
import org.apache.commons.validator.routines.DateValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.j256.ormlite.dao.Dao;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.Window;
import ru.codeuniverse.audiocrammer.model.AudioRecord;
import ru.codeuniverse.audiocrammer.model.AudioRecordDAOManager;
import ru.codeuniverse.audiocrammer.model.Importance;
import ru.codeuniverse.audiocrammer.model.ReviewTracker;
import ru.codeuniverse.audiocrammer.model.Settings;
import ru.codeuniverse.audiocrammer.utility.Utilities;

public class AdjustRecordViewController implements Initializable {

	private static final Logger logger = LoggerFactory
			.getLogger(AdjustRecordViewController.class);

	AudioRecord audioRecordToAdjust;

	// Stage for this Controller
	private Stage adjustStage;

	OutputStream mPlayerOutputStream;

	@FXML
	private BorderPane rootBorderPane;

	@FXML
	private TextField idTextField;

	// Audio Player

	@FXML
	private Button playBtn;

	boolean isPlayBtnPressedFirstTime = true;

	// speed control

	@FXML
	private Slider playRewindSlider;

	@FXML
	private Slider playSpeedSlider;

	@FXML
	private CheckBox presetPlaySpeedChkBox;

	// Prev / Next

	@FXML
	private Button prevBtn;

	@FXML
	private Button nextBtn;

	// Importance / Rep Frequency

	@FXML
	private RadioButton highRadioBtn;

	@FXML
	private ToggleGroup ImportanceToggleGroup;

	@FXML
	private RadioButton lowRadioBtn;

	@FXML
	private RadioButton midRadioBtn;

	// Cram / Discontinue / Start Again/Anew

	@FXML
	private CheckBox cramCheckBox;

	@FXML
	private CheckBox discontinueCheckBox;

	@FXML
	private CheckBox startAnewCheckBox;

	// Review Date - related

	@FXML
	private TextField nextReviewDateTextField;

	@FXML
	private TextField nextReviewDateInDaysTextField;

	@FXML
	private TextField adjustedNextReviewInDaysTextField;

	@FXML
	private TextField nextReviewNumberTextField;

	// Topics

	@FXML
	private TextField subTopicTextField;

	@FXML
	private TextField topicTextField;

	@FXML
	private TextField topicGroupTextField;

	// Search tags

	@FXML
	private TextField searchTagsTextField;

	// Comment

	@FXML
	private TextArea CommentTextArea;

	// OTHER

	@FXML
	private TextField trackedNameTextField;

	@FXML
	private TextField origModifiedTimeTextField;

	@FXML
	private TextField checkoutDateTextField;

	@FXML
	private TextField fileSizeTextField;

	@FXML
	private TextField durationTextField;

	@FXML
	private TextArea actualReviewHistoryTextField;

	@FXML
	private TextField adjustedNextReviewISODateTextField;

	@FXML
	private Label discontinueLabel;

	@FXML
	private Label searchTagsLabel;

	@FXML
	private Label fileSizeLabel;

	@FXML
	private Label nextReviewDateInDaysLabel;

	@FXML
	private Label nextReviewDateInDaysAdjustedLabel;

	// play sound of file BUTTON Pressed
	@FXML
	void playBtnAction(ActionEvent event) {
		if (!isPlayBtnPressedFirstTime) {
			// send pause text=command to console for MPlayer in slave mode
			togglePauseResumeMPlayer();
		} else {
			// start playint - Play Btn pressed first time.
			// Any next Press is just pause/resume
			Settings settings = new Settings();
			String trackedAudioPath = settings.getTrackedAudioDir();
			String fileName = audioRecordToAdjust.getTrackedName();
			String audioRecordFileWithPath = trackedAudioPath + "\\" + fileName;

			startPlayingMPlayer(audioRecordFileWithPath);
		}
	}

	// MPlayer CONTROL

	private void startPlayingMPlayer(String audioRecordFileWithPath) {
		try {
			// ProcessBuilder builder = new ProcessBuilder("N:\\MPlayer\\mplayer.exe",
			// "-quiet",
			// "-slave", "P:\\intro1_1.mp3");

			Settings settings = new Settings();
			String mPlayerPath = settings.getMPlayerDir();
			Path mPlayerExePath = Paths.get(mPlayerPath, "mplayer.exe");
			String mPlayerExeStr = mPlayerExePath.toString(); // "N:\\MPlayer\\mplayer.exe" on Win
			logger.info("startPlayingMPlayer():  mPlayerExeStr = " + mPlayerExeStr);
			//XXX check if MPlayer directory is correct (contains mplayer.exe)
			ProcessBuilder builder = new ProcessBuilder(mPlayerExeStr, "-quiet",
					"-slave", audioRecordFileWithPath);
			Process p = builder.start();

			mPlayerOutputStream = p.getOutputStream();

			isPlayBtnPressedFirstTime = false;

		} catch (IOException ioe) {
			logger.error("playBtnAction() called first time got IOException: " + ioe);
		}
	}

	private void togglePauseResumeMPlayer() {
		if (mPlayerOutputStream != null) {
			try {
				mPlayerOutputStream.write("pause\n".getBytes());
				mPlayerOutputStream.flush();
			} catch (IOException ioe) {
				logger.error(
						"playBtnAction() in if (!isPlayBtnPressedFirstTime) branch got IOException: "
								+ ioe);
			}
		}
	}

	private void rewindFFMPlayer(int seekValue) {
		logger.info("rewindFFMPlayer() called with seekValue = " + seekValue);
		if (mPlayerOutputStream != null) {
			try {
				String commandStr = "seek " + String.valueOf(seekValue) + " 1\n";
				logger.info("rewindFFMPlayer() commandStr = " + commandStr);
				mPlayerOutputStream.write(commandStr.getBytes());
				mPlayerOutputStream.flush();
			} catch (IOException ioe) {
				logger.error("rewindFFMPlayer() with seekValue = " + seekValue
						+ " got IOException: " + ioe);
			}
		}
	}

	/**
	 * stops playing AudioRecord associated with this window and closes OutputStream for
	 * communication with MPlayer in Console in slave mode
	 */
	public void stopMPlayer() {
		if (mPlayerOutputStream != null) {
			try {
				mPlayerOutputStream.write("stop\n".getBytes());
				mPlayerOutputStream.flush();
				mPlayerOutputStream.close();
			} catch (IOException ioe) {
				logger.error("stopMPlayer() got IOException: " + ioe);

			}
		}
	}

	/**
	 * exits (quits) MPlayer.
	 */
	public void quitMPlayer() {
		if (mPlayerOutputStream != null) {
			try {
				mPlayerOutputStream.write("quit\n".getBytes());
				mPlayerOutputStream.flush();
				mPlayerOutputStream.close();
			} catch (IOException ioe) {
				logger.error("quitMPlayer() got IOException: " + ioe);

			}
		}
	}

	// PLAY Sliders (Rewind / Speed) - onKEY

	@FXML
	void playRewindSliderOnKeyReleased(KeyEvent event) {
		logger.info("SLIDER TRIGGERED by sliderOnKeyReleased() Listener: pos = "
				+ playRewindSlider.getValue());
		int seekValue = (int) playRewindSlider.getValue();
		rewindFFMPlayer(seekValue);
	}

	@FXML
	void playSpeedSliderOnKeyReleased(KeyEvent event) {
		logger.info("PLAY SPEED SLIDER TRIGGERED BY playSpeedSliderOnKeyReleased(): pos = "
				+ playSpeedSlider.getValue());

		double speedMultiplier = playSpeedSlider.getValue();
		logger.info("playSpeed_SLIDER seekValue = " + speedMultiplier);

		adjustPlaySpeedMPlayer(speedMultiplier);

		// unselect Speed 1.7 checkbox if Slider set not to 1.7
		if (playSpeedSlider.getValue() != 1.7) {
			presetPlaySpeedChkBox.setSelected(false);
		}
	}

	@FXML
	void nextBtnAction(ActionEvent event) {
		quitMPlayer();

		// set next audio-file
		FXMain.mainViewController.setRecordToAdjustAtNext();

		Window thisWindow = ((Node) event.getSource()).getScene().getWindow();
		Stage thisStage = (Stage) thisWindow;
		thisStage.hide();

		// show adjust wnd of next audio-file
		FXMain.mainViewController.createInitAndShowAdjustStage();

	}

	@FXML
	void prevBtnAction(ActionEvent event) {
		quitMPlayer();

		// set prev audio-file
		FXMain.mainViewController.setRecordToAdjustAtPrevious();

		Window thisWindow = ((Node) event.getSource()).getScene().getWindow();
		Stage thisStage = (Stage) thisWindow;
		thisStage.hide();

		// show adjust wnd of prev audio-file
		FXMain.mainViewController.createInitAndShowAdjustStage();
	}

	// OTHER BUTTON-PRESS (not Play)

	/**
	 * Saves changes to DB
	 */
	@FXML
	void applyBtnAction(ActionEvent event) {
		quitMPlayer();
		audioRecordToAdjust.setComment(CommentTextArea.getText().trim());
		audioRecordToAdjust.setSearchTags(searchTagsTextField.getText().trim());
		audioRecordToAdjust.setSubTopic(subTopicTextField.getText().trim());

		if (discontinueCheckBox.isSelected()) {
			ReviewTracker reviewTracker;
			try {
				reviewTracker = new ReviewTracker();
				reviewTracker.discontinue(audioRecordToAdjust);
				FXMain.mainViewController.updateStatusBar();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				logger.info(
						"applyBtnAction(): SQL Exception  trying to discontinue audioRecord");
				e.printStackTrace();
			} catch (Exception e) {
				logger.info("applyBtnAction(): Exception trying to discontinue audioRecord");
				e.printStackTrace();
			}

			Window wnd = ((Node) event.getSource()).getScene().getWindow();
			Stage adjustRecordStage = (Stage) wnd;
			adjustRecordStage.hide();
			return; // discontinued => nothing else matters!
		}

		if (cramCheckBox.isSelected()) {
			audioRecordToAdjust.setCramFlag(true);
			try {
				copyFileFromTrackedToCrammingDir(audioRecordToAdjust);
			} catch (IOException e) {
				logger.info("Cannot copy file to cram from TrackedAudioDir to CrammingDir");
			}
		} else {
			audioRecordToAdjust.setCramFlag(false);
			// remove file from Cram Folder if it is there
			Settings settings = new Settings();
			String fileName = audioRecordToAdjust.getTrackedName();
			String crammingDir = settings.getCramThisOutputDir();
			Path filePathInCrammingDir = Paths.get(crammingDir, fileName);
			try {
				Files.deleteIfExists(filePathInCrammingDir);
			} catch (IOException e) {
				logger.info(
						"Cannot delete file from crammingDir (cram chrckbox was unchecked)");
			}
		}

		if (midRadioBtn.isSelected()) {
			audioRecordToAdjust.setImportance(Importance.MID);
		} else if (highRadioBtn.isSelected()) {
			audioRecordToAdjust.setImportance(Importance.HIGH);
		} else if (lowRadioBtn.isSelected()) {
			audioRecordToAdjust.setImportance(Importance.LOW);
		}

		// sets audioRecordToAdjust.setNextReviewDate
		adjustNextReviewDate();

		/* Updates database so that AudioRecord as if checked-out from Backlog first time - sets
		 * nextReviewNumber=1, nextReviewDate=3 (whatever 1st repeat interval is), adds "0000-00-00"
		 * ToReviewHistory (start-anew marker). Changing this in Database is enough to start repeating
		 * anew. 
		 */
		// THIS BLOCK SHAlong GO AFTER adjustNextReviewDate() METHOD CALL!!!
		if (startAnewCheckBox.isSelected()) {
			LocalDate today = LocalDate.now();
			LocalDate todayPlusOneDay = today.plusDays(1);			
			String nextReviewISODate = todayPlusOneDay.format(DateTimeFormatter.ISO_LOCAL_DATE);

			//audioRecordToAdjust.setChekoutDate(todayDateISO);			
			audioRecordToAdjust.setNextReviewDate(nextReviewISODate);
			// as if just checked-out from Backlog
			audioRecordToAdjust.setNextReviewNumber(0);
			// start-anew marker
			audioRecordToAdjust.addToReviewHistory("0000-00-00");
		}

		// write all changes to DB
		AudioRecordDAOManager audioRecordDAOManager;
		Dao<AudioRecord, Integer> audioRecordDAO;
		try {
			audioRecordDAOManager = new AudioRecordDAOManager();
			audioRecordDAO = audioRecordDAOManager.getORMLiteDAO();
			audioRecordDAO.update(audioRecordToAdjust);
		} catch (SQLException e1) {
			logger.error("applyBtnAction() got SQLException updating AudioRecord: " + e1);

		}

		Window wnd = ((Node) event.getSource()).getScene().getWindow();
		Stage adjustRecordStage = (Stage) wnd;
		adjustRecordStage.hide();
	}

	// returns simple file name (w/o path) of copied file
	private String copyFileFromTrackedToCrammingDir(AudioRecord audioRecord)
			throws IOException {
		String fileName = audioRecord.getTrackedName();
		Settings settings = new Settings();
		String trackedAudioDir = settings.getTrackedAudioDir();
		Path trackedPathName = Paths.get(trackedAudioDir, fileName);
		String crammingDir = settings.getCramThisOutputDir();
		Path crammingPathName = Paths.get(crammingDir, fileName);
		try {
			Files.copy(trackedPathName, crammingPathName, StandardCopyOption.COPY_ATTRIBUTES);

		} catch (FileAlreadyExistsException e) {
			logger.info("Cannot copy file from trackedAudioDir to crammingDir");
		}

		return fileName;
	}

	@FXML
	void cancelBtnAction(ActionEvent event) {
		quitMPlayer();
		Window wnd = ((Node) event.getSource()).getScene().getWindow();
		Stage adjustRecordStage = (Stage) wnd;
		adjustRecordStage.hide();
	}

	@FXML
	void OnAdjustedNextReviewInDaysTextFieldChange(KeyEvent event) {
		if (!isNextReviewInDaysInputValid()) {
			showInvalidNextReviewInDaysInputMessage();
			adjustedNextReviewInDaysTextField.clear();
		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		initToolTips();

		audioRecordToAdjust = FXMain.mainViewController.getRecordToAdjust();

		// SHALL GO AFTER audioRecordToAdjust INITIALIZED!
		initPlayRewindSlider();
		initPlaySpeedSlider();
		initPresetPlaySpeedChkBox();

		trackedNameTextField.setText(audioRecordToAdjust.getTrackedName());
		idTextField.setText(String.valueOf(audioRecordToAdjust.getId()));

		Importance importance = audioRecordToAdjust.getImportance();
		if (importance == Importance.MID) {
			midRadioBtn.setSelected(true);
		} else if (importance == Importance.HIGH) {
			highRadioBtn.setSelected(true);
		} else if (importance == Importance.LOW) {
			lowRadioBtn.setSelected(true);
		}

		boolean isCramEnabled = audioRecordToAdjust.isCramFlag();
		if (isCramEnabled) {
			cramCheckBox.setSelected(true);
		} else {
			cramCheckBox.setSelected(false);
		}

		String nextReviewDate = audioRecordToAdjust.getNextReviewDate();
		nextReviewDateTextField.setText(nextReviewDate);

		String nextReviewNumber = String.valueOf(audioRecordToAdjust.getNextReviewNumber());
		nextReviewNumberTextField.setText(nextReviewNumber);

		String searchTags = audioRecordToAdjust.getSearchTags();
		searchTagsTextField.setText(searchTags);

		String subTopic = audioRecordToAdjust.getSubTopic();
		subTopicTextField.setText(subTopic);

		String topic = audioRecordToAdjust.getTopic();
		topicTextField.setText(topic);

		String topicGroup = audioRecordToAdjust.getTopicGroup();
		topicGroupTextField.setText(topicGroup);

		String comment = audioRecordToAdjust.getComment();
		CommentTextArea.setText(comment);

		String actualReviewHistory = audioRecordToAdjust.getActualReviewHistory();
		actualReviewHistoryTextField.setText(actualReviewHistory);

		String fileSizeInMegabytes;
		double bytesInMegabyte = 1024.0 * 1024.0;
		double fileSizeInMegabytesDouble = audioRecordToAdjust.getFileSize() / bytesInMegabyte;
		fileSizeInMegabytes = String.valueOf(Precision.round(fileSizeInMegabytesDouble, 1));
		fileSizeTextField.setText(fileSizeInMegabytes);

		String origModifiedTime = audioRecordToAdjust.getOrigModifiedTime();
		origModifiedTimeTextField.setText(origModifiedTime);

		String durationInMinutes = String.valueOf(Utilities.roundSpecial(
				(float) audioRecordToAdjust.getFileSize() / Settings.MINUTE_HAS_BYTES));
		durationTextField.setText(durationInMinutes);

		int nDays = calculateDaysUntilNextReview();
		nextReviewDateInDaysTextField.setText(String.valueOf(nDays));
		
		String checkoutNewDate = audioRecordToAdjust.getChekoutDate();
		checkoutDateTextField.setText(checkoutNewDate);

		// on ESC close adjustRecord form and do nothing
		rootBorderPane.addEventHandler(KeyEvent.KEY_RELEASED, (KeyEvent event) -> {
			if (KeyCode.ESCAPE == event.getCode()) {
				quitMPlayer();
				Window wnd = ((Node) event.getSource()).getScene().getWindow();
				Stage adjustRecordStage = (Stage) wnd;
				adjustRecordStage.hide();
			}
		});

	}

	/**
	 * add Listener to Play rewind Slider
	 */
	private void initPlayRewindSlider() {

		double durationInMin = (double) audioRecordToAdjust.getFileSize()
				/ Settings.MINUTE_HAS_BYTES;
		if (durationInMin < 1.5) {
			playRewindSlider.setMax(70);
		} else if (durationInMin < 2.5) {
			playRewindSlider.setMax(80);
		} else if (durationInMin < 3.5) {
			playRewindSlider.setMax(90);
		} else if (durationInMin > 7) {
			playRewindSlider.setMax(95);
		}

		// to fire once when moving slider stopped
		playRewindSlider.setOnMouseReleased(event -> {
			logger.info("SLIDER TRIGGERED BY setOnMouseReleased(): pos = "
					+ playRewindSlider.getValue());

			int seekValue = (int) playRewindSlider.getValue();
			logger.info("SLIDER seekValue = " + seekValue);

			rewindFFMPlayer(seekValue);
		});
	}

	private void initPlaySpeedSlider() {
		// fire once when moving slider stopped
		playSpeedSlider.setOnMouseReleased(event -> {
			logger.info("PLAY SPEED SLIDER TRIGGERED BY setOnMouseReleased(): pos = "
					+ playSpeedSlider.getValue());

			double speedMultiplier = playSpeedSlider.getValue();
			logger.info("playSpeed_SLIDER seekValue = " + speedMultiplier);

			adjustPlaySpeedMPlayer(speedMultiplier);

			// unselect Speed 1.7 checkbox if Slider set not to 1.7
			if (playSpeedSlider.getValue() != 1.7) {
				presetPlaySpeedChkBox.setSelected(false);
			}
		});
	}

	private void initPresetPlaySpeedChkBox() {
		final double PRESET_SPEED = 1.7;

		presetPlaySpeedChkBox.selectedProperty().addListener(new ChangeListener() {
			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				logger.info("presetPlaySpeedChkBox Listener called");
				if ((Boolean) newValue) {
					logger.info("presetPlaySpeedChkBox Listener ADJUSTED PLAY SPEED TO "
							+ PRESET_SPEED);
					// changing slider value don't trigger MouseMove & OnKeyPress
					playSpeedSlider.setValue(PRESET_SPEED);
					adjustPlaySpeedMPlayer(PRESET_SPEED);
				} else {
					// adjust speed as per Speed Slider
					adjustPlaySpeedMPlayer(playSpeedSlider.getValue());
				}
			}
		});

	}

	private void adjustPlaySpeedMPlayer(double speedMultiplier) {
		logger.info(
				"adjustPlaySpeedMPlayer() called with speedMultiplier = " + speedMultiplier);
		if (mPlayerOutputStream != null) {
			try {
				if ((speedMultiplier - 1.0) < 0.1) {
					mPlayerOutputStream.write("af_clr\n".getBytes());
					mPlayerOutputStream.flush();
					logger.info("adjustPlaySpeedMPlayer() with speedMultiplier = "
							+ speedMultiplier + " set speed to normal (speed_set 1.0) ");
					return; // do nth more
				} else {
					// set normal speed
					//
					mPlayerOutputStream.write("af_clr\n".getBytes());
					mPlayerOutputStream.flush();

					// set % speed of normal speed, PRESERVING same pitch!
					// slave mode command syntax is:
					// af_add scaletempo=scale=1.8:speed=tempo
					// RESET: af_clr
					String commandStr = "af_add scaletempo=scale="
							+ String.valueOf(speedMultiplier) + ":speed=tempo\n";
					mPlayerOutputStream.write(commandStr.getBytes());
					mPlayerOutputStream.flush();
					logger.info("adjustPlaySpeedMPlayer() commandStr = " + commandStr);
				}
			} catch (IOException ioe) {
				logger.error("adjustPlaySpeedMPlayer() with speedMultiplier = "
						+ speedMultiplier + " got IOException: " + ioe);
			}
		}
	}

	private void initToolTips() {
		Tooltip discontinueToolTip = new Tooltip();
		String discontinueToolTipText = "Deletes record from DB and Tracked Audio Dir, "
				+ "and moves it to Discontinued Folder. " + "DOES NOT DELETE FROM CRAM FOLDER, "
				+ "SO UNCHECK CRAM FIRST, IF NEED TO DELETE FROM CRAM FOLDER!";
		discontinueToolTip.setText(discontinueToolTipText);
		discontinueCheckBox.setTooltip(discontinueToolTip);
		discontinueLabel.setTooltip(discontinueToolTip);

		Tooltip cramToolTip = new Tooltip();
		String cramToolTipText = "Checking this box - adds (unchecking - deletes) file from Cramming Folder. Works only if file is tracked (not discontinued). Does not affect tracked folder.";
		cramToolTip.setText(cramToolTipText);
		cramCheckBox.setTooltip(cramToolTip);

		Tooltip searchTagsToolTip = new Tooltip();
		String searchTagsToolTipText = "Comma separated. Leading and trailing whitespaces (before and after each comma) ignored";
		searchTagsToolTip.setText(searchTagsToolTipText);
		searchTagsTextField.setTooltip(searchTagsToolTip);
		searchTagsLabel.setTooltip(searchTagsToolTip);

		Tooltip fileSizeToolTip = new Tooltip();
		String fileSizeToolTipText = "In real MegaBytes (MibiBytes)";
		fileSizeToolTip.setText(fileSizeToolTipText);
		fileSizeTextField.setTooltip(fileSizeToolTip);
		fileSizeLabel.setTooltip(fileSizeToolTip);

		Tooltip nextReviewNumberToolTip = new Tooltip();
		String nextReviewNumberToolTipText = "Check-out itself is zero, so after checkout next review number is 1 (first time you review after check-out study)";
		nextReviewNumberToolTip.setText(nextReviewNumberToolTipText);
		nextReviewNumberTextField.setTooltip(nextReviewNumberToolTip);

		Tooltip nextReviewDateInDaysToolTip = new Tooltip();
		String nextReviewDateInDaysToolTipText = "Counts full days in-between: not including last checkout\review day and next review day";
		nextReviewDateInDaysToolTip.setText(nextReviewDateInDaysToolTipText);
		nextReviewDateInDaysTextField.setTooltip(nextReviewDateInDaysToolTip);
		nextReviewDateInDaysLabel.setTooltip(nextReviewDateInDaysToolTip);

		Tooltip adjustedNextReviewToolTip = new Tooltip();
		String adjustedNextReviewToolTipText = "number of full days in-between from today (not counting today and reviewDay";
		adjustedNextReviewToolTip.setText(adjustedNextReviewToolTipText);
		adjustedNextReviewInDaysTextField.setTooltip(adjustedNextReviewToolTip);
		nextReviewDateInDaysAdjustedLabel.setTooltip(adjustedNextReviewToolTip);
	}

	private void adjustNextReviewDate() {

		boolean isDaysTextFieldNotEmpty = !adjustedNextReviewInDaysTextField.getText().trim()
				.isEmpty();
		boolean isISODateTextFieldNotEmpty = !adjustedNextReviewISODateTextField.getText()
				.trim().isEmpty(); // adjustedNextReviewISODateTextField

		if (isDaysTextFieldNotEmpty && isISODateTextFieldNotEmpty) {
			showAmbiguousNextReviewInputMessage();
			return;
		}

		if (isISODateTextFieldNotEmpty) {
			if (!isISOReviewDateInputValid()) {
				showISOReviewDateInputInvalidMessage();
				return;
			}
			audioRecordToAdjust
					.setNextReviewDate(adjustedNextReviewISODateTextField.getText().trim());
			return;
		}

		// otherwise convert nDays from today to yyyy-MM-double ISO 8601
		String nextReviewISODate = audioRecordToAdjust.getNextReviewDate();
		Date nextReviewDateAsDate = null;
		try {
			nextReviewDateAsDate = convertISOToDate(nextReviewISODate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String dateStr = adjustedNextReviewInDaysTextField.getText();
		int plusDays = 0;
		if (!dateStr.isEmpty() && dateStr.matches("\\d+")) {
			plusDays = Integer.valueOf(adjustedNextReviewInDaysTextField.getText());
		} else {
			return;
		}

		String ISONextReviewAdjustedDate;
		Calendar calendar = Calendar.getInstance();
		Date today = calendar.getTime();
		// +1, I need [today, plusDays, TargetDate]
		Date adjustedReviewDateAsDate = NDaysPlusDate(today, plusDays + 1);
		ISONextReviewAdjustedDate = DateFormatUtils.ISO_DATE_FORMAT
				.format(adjustedReviewDateAsDate);

		audioRecordToAdjust.setNextReviewDate(ISONextReviewAdjustedDate);
	}

	private void showISOReviewDateInputInvalidMessage() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("INVALID NEXT REVIEW ISO DATE INPUT!");
		alert.setHeaderText(
				"Next review date was not changed, all other changes were applied!");
		alert.setContentText("Only digits allowed and pattern shall be like this: 2017-02-28");
		alert.showAndWait();
	}

	private boolean isISOReviewDateInputValid() {
		String input = adjustedNextReviewISODateTextField.getText().trim();

		if (input.isEmpty()) {
			return true;
		}

		// year cannot be less than 2017
		if (input.substring(0, 4).matches("[0-9]{4}")) {
			if (Integer.valueOf(input.substring(0, 4)) < 2017) {
				return false;
			}
		} else {
			return false;
		}

		DateValidator dateValidator = new DateValidator();
		Date parsedDate = dateValidator.validate(input, "yyyy-MM-dd");
		boolean matchesISOPattern = (parsedDate == null) ? false : true;

		// we intentionally don't check if date goes before today
		// if user sets such early date, it means he requests immediate review
		// so later card would be scheduled for review as soon as possible
		return matchesISOPattern;
	}

	private void showInvalidNextReviewInDaysInputMessage() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("INVALID NEXT REVIEW IN DAYS INPUT!");
		alert.setHeaderText(
				"Invalid input: interval in days must contain only digits, shall be integer and cannot be zero or negative.");
		alert.setContentText("Please try again to input correct value!");
		alert.showAndWait();
	}

	private boolean isNextReviewInDaysInputValid() {
		String input = adjustedNextReviewInDaysTextField.getText().trim();
		if (input.isEmpty()) {
			return true;
		}
		boolean isNumeric = StringUtils.isNumeric(input); // 2.3 returns false
		if (!isNumeric) {
			return false;
		}
		int numericInput = Integer.valueOf(input);
		boolean isPositive = numericInput > 0;
		boolean isInputValid = isNumeric && isPositive;

		return isInputValid;
	}

	private void showAmbiguousNextReviewInputMessage() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("AMBIGUOUS NEXT REVIEW INPUT!");
		alert.setHeaderText(
				"Next review date WAS NOT CHANGED! All OTHER CHANGES were APPLIED!");
		alert.setContentText(
				"Input either days from now interval, or ISO date of next review. Cannot set both days from now interval, and ISO date of next review. Set only one of them!");
		alert.showAndWait();
	}

	/**
	 * @return number of days, not including last checkout\review day and next review day
	 *         (counts full days strictly in-between)
	 */
	private int calculateDaysUntilNextReview() {
		String nextReviewDate;
		Calendar calendar = Calendar.getInstance();
		Date today = calendar.getTime();

		nextReviewDate = audioRecordToAdjust.getNextReviewDate();
		Date nextReview = null;
		try {
			nextReview = convertISOToDate(nextReviewDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// if nextReview not scheduled, return 0
		if (nextReview == null) {
			return 0;
		}
		int daysDifference = (int) getDaysBetweenDates(today, nextReview);

		return daysDifference;
	}

	/**
	 * @param earlier
	 *            - earlier Date (say, Jan 12)
	 * @param later
	 *            - later Date (say, Jan 26)
	 * @return how many days (24 hours) are between 2 Dates
	 */
	private long getDaysBetweenDates(Date earlier, Date later) {
		long diff = later.getTime() - earlier.getTime();
		return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}

	/**
	 * @param String
	 *            with ISO 8601 date in pattern yyyy-MM-dd
	 * @return Date
	 * @throws ParseException
	 */
	public Date convertISOToDate(String ISO_yyyy_MM_dd_String) throws ParseException {
		if (ISO_yyyy_MM_dd_String == null) {
			return null;
		}

		Date date = DateUtils.parseDate(ISO_yyyy_MM_dd_String, new String[] { "yyyy-MM-dd" });
		return date;
	}

	private Date NDaysPlusDate(Date date, int nDays) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_MONTH, nDays);

		return new Date(cal.getTimeInMillis());
	}
}
