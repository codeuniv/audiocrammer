package ru.codeuniverse.audiocrammer.view;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.stage.Window;
import ru.codeuniverse.audiocrammer.model.Settings;

public class SettingsViewController implements Initializable {

	private static final Logger logger = LoggerFactory.getLogger(SettingsViewController.class);

	@FXML
	private AnchorPane rootAnchorPane;

	@FXML
	private Button openOutputDirBtn;

	@FXML
	private Label internalDirLbl;

	// app settings
	private Settings settings;

	@FXML
	private TextField maxDurationTextField;

	@FXML
	private TextField maxNFilesTextField;

	@FXML
	private CheckBox useNFilesCheckBox;

	@FXML
	private CheckBox useDurationCheckBox;
	
	@FXML
	private CheckBox useAllFilesCheckBox;

	@FXML
	private Label todayAudioFolderLabel;

	@FXML
	private Label todayAudioSavedMsgLabel;

	@FXML
	private TextField mPlayerDirTextField;

	@FXML
	void applyBtnAction(ActionEvent event) {

		// validate and save maxDurationTextField
		String maxDuration = maxDurationTextField.getText();
		if (isMaxDurationValid(maxDuration)) {
			String durationToSet = maxDurationTextField.getText();
			int maxDurationToSet = Integer.valueOf(durationToSet);
			settings.setMaxDuration(maxDurationToSet);
		} else {
			showMsgMaxDurationIsNotValidNumeric();
			return;
		}

		// validate and save maxNFilesTextField
		String maxNFiles = maxNFilesTextField.getText().trim();
		if (isMaxNFilesValid(maxNFiles)) {
			int maxNFilesToSet = Integer.valueOf(maxNFiles);
			settings.setMaxNFiles(maxNFilesToSet);
		} else {
			showMsgMaxNFilesIsNotValidNumeric();
			return;
		}

		// set MPlayer dir
		settings.setMPlayerDir(mPlayerDirTextField.getText().trim());
		
		// set shallUseDuration / shallUseNFiles CheckBoxes 
		settings.setShallUseDuration(!useDurationCheckBox.isDisabled());
		settings.setShallUseNFiles(!useNFilesCheckBox.isDisabled());
		// check if shallUseDuration and shallUseNFiles are mutually exclusive (true/false or false/true)
		if ((useDurationCheckBox.isDisabled() ^ useNFilesCheckBox.isDisabled()) != true) {
			logger.error("SettingsViewController.init(): shallUseDuration and shallUseNFiles not mutually exclusive (true/false or false/true)! Which one to use?");
		} 
		
		settings.setShallUseAllAvailableFiles(useAllFilesCheckBox.isSelected());

		// close window
		Window wnd = ((Node) event.getSource()).getScene().getWindow();
		Stage settingsStage = (Stage) wnd;
		settingsStage.hide();
		return;
	}

	private boolean isMaxDurationValid(String maxDuration) {
		boolean isInputNumeric;
		boolean isInputGreaterOrEqualMinLimit;
		isInputNumeric = StringUtils.isNumeric(maxDuration);

		if (Integer.valueOf(maxDuration) >= Settings.MAX_DURATION_MIN_LIMIT) {
			isInputGreaterOrEqualMinLimit = true;
		} else {
			isInputGreaterOrEqualMinLimit = false;
		}

		if (isInputNumeric && isInputGreaterOrEqualMinLimit) {
			return true;
		} else {
			return false;
		}
	}

	private boolean isMaxNFilesValid(String maxNFiles) {
		boolean isInputNumeric;
		boolean isInputGreaterOrEqualMinLimit;
		isInputNumeric = StringUtils.isNumeric(maxNFiles);

		if (Integer.valueOf(maxNFiles) >= Settings.MAX_N_FILES_MIN_LIMIT) {
			isInputGreaterOrEqualMinLimit = true;
		} else {
			isInputGreaterOrEqualMinLimit = false;
		}

		if (isInputNumeric && isInputGreaterOrEqualMinLimit) {
			return true;
		} else {
			return false;
		}
	}

	public SettingsViewController() {
		settings = new Settings();

	}

	@FXML
	void openTodayStudyAudioDirBtnAction(ActionEvent event) {
		if (SystemUtils.IS_OS_WINDOWS) {
			Runtime rt = Runtime.getRuntime();
			String folderToOpen = settings.getTodayStudyAudioOutputDir();
			String command = "explorer.exe " + folderToOpen;
			try {
				rt.exec(command);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	@FXML
	void openCramThisDirBtnAction(ActionEvent event) {
		if (SystemUtils.IS_OS_WINDOWS) {
			Runtime rt = Runtime.getRuntime();
			String folderToOpen = settings.getCramThisOutputDir();
			String command = "explorer.exe " + folderToOpen;
			try {
				rt.exec(command);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	@FXML
	void onSelectMPlayerDir(ActionEvent event) {
		Window ownerWindow = ((Button) event.getSource()).getScene().getWindow();
		File dirOfMPlayer = getMPlayerDirectoryByAskingUser(ownerWindow);
		mPlayerDirTextField.setText(dirOfMPlayer.getPath());
	}

	@FXML
	void maxDurationKeyTypedHandler(KeyEvent event) {

	}

	@FXML
	void maxDurationKeyReleasedHandler(KeyEvent event) {

	}
	
	
    @FXML
    void onUseMaxDurationCheckBoxAction(ActionEvent event) {
       	// toggle disabled state of another CheckBox: useAllFilesCheckBox
    	if (useDurationCheckBox.isSelected()) {
    		useAllFilesCheckBox.setSelected(false);
    		useAllFilesCheckBox.setDisable(true);
		} else {
			useAllFilesCheckBox.setDisable(false);
		}
    	// toggle disabled state of another CheckBox: useNFilesCheckBox
    	if(useNFilesCheckBox.isDisabled()) {    		
    		useNFilesCheckBox.setDisable(false);
    	} else {  
    		// useNFilesCheckBox was enabled
    		useNFilesCheckBox.setSelected(false);
    		useNFilesCheckBox.setDisable(true);
    	}
    }

    @FXML
    void onUseNFilesCheckBoxAction(ActionEvent event) {
    	// toggle disabled state of another CheckBox: useAllFilesCheckBox
    	if (useNFilesCheckBox.isSelected()) {
    		useAllFilesCheckBox.setSelected(false);
    		useAllFilesCheckBox.setDisable(true);
		} else {
			useAllFilesCheckBox.setDisable(false);
		}
       	// toggle disabled state of another CheckBox: useDurationCheckBox
    	if(useDurationCheckBox.isDisabled()) {
    		useDurationCheckBox.setDisable(false);
    	} else {
    		useDurationCheckBox.setSelected(false);
    		useDurationCheckBox.setDisable(true);
    	}
    }
    
    
    
    @FXML
    void onUseAllFilesCheckBoxAction(ActionEvent event) {
    	// toggle disabled state of another CheckBoxes: 
    	// useDurationCheckBox and  useNFilesCheckBox
    	if(useAllFilesCheckBox.isDisabled()) {
    		useDurationCheckBox.setDisable(false);
    		useAllFilesCheckBox.setDisable(false);
    	} else if(useAllFilesCheckBox.isSelected()){
    		useDurationCheckBox.setSelected(false);
    		useDurationCheckBox.setDisable(true);
    		useNFilesCheckBox.setSelected(false);
    		useNFilesCheckBox.setDisable(true);
    	} else {
    		// useAllFilesCheckBox is enabled and unselected
    		useDurationCheckBox.setSelected(false);
    		useDurationCheckBox.setDisable(false);
    		useNFilesCheckBox.setSelected(false);
    		useNFilesCheckBox.setDisable(false);
    	}
    }


	@Override
	public void initialize(URL location, ResourceBundle resources) {
		logger.debug("Entered SettingsViewController initialize() method");
		String internalFolderName = settings.getAppDir();
		String text = internalDirLbl.getText() + "  " + internalFolderName;
		internalDirLbl.setText(text);

		String maxDuration = String.valueOf(settings.getMaxDuration());
		maxDurationTextField.setText(maxDuration);

		String maxNFiles = String.valueOf(settings.getMaxNFiles());
		maxNFilesTextField.setText(maxNFiles);

		String todayAudioFolder = settings.getTodayStudyAudioOutputDir();
		todayAudioFolderLabel.setText(todayAudioFolder);

		mPlayerDirTextField.setText(settings.getMPlayerDir());
		
		useDurationCheckBox.setSelected(settings.getShallUseDuration());
		useNFilesCheckBox.setSelected(settings.getShallUseNFiles());
		useAllFilesCheckBox.setSelected(settings.getShallUseAllAvailableFiles());

		// on ESC close adjustRecord form and do nothing
		rootAnchorPane.addEventHandler(KeyEvent.KEY_RELEASED, (KeyEvent event) -> {
			if (KeyCode.ESCAPE == event.getCode()) {
				Window wnd = ((Node) event.getSource()).getScene().getWindow();
				Stage settingsStage = (Stage) wnd;
				settingsStage.hide();
			}
		});
	}

	// positive numeric value >= 25 (settings-dependent) is allowed
	private void showMsgMaxDurationIsNotValidNumeric() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("PLEASE ENTER A VALID NUMBER");
		String headerText = "Please enter a positive number >= "
				+ settings.MAX_DURATION_MIN_LIMIT;
		alert.setHeaderText(headerText);
		alert.setContentText(headerText);
		alert.showAndWait();
	}

	// positive numeric value >= 1 (settings-dependent) is allowed
	private void showMsgMaxNFilesIsNotValidNumeric() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("PLEASE ENTER A VALID NUMBER");
		String headerText = "Please enter a positive number >= "
				+ settings.MAX_N_FILES_MIN_LIMIT;
		alert.setHeaderText(headerText);
		alert.setContentText(headerText);
		alert.showAndWait();
	}

	@FXML
	void selectTodayAudioFolderBtnHandler(ActionEvent event) {

		Node source = (Node) event.getSource();
		Stage thisStage = (Stage) source.getScene().getWindow();

		DirectoryChooser directoryChooser = new DirectoryChooser();
		File selectedDirectory = directoryChooser.showDialog(thisStage);

		if (selectedDirectory == null) {
			// do nothing
		} else {
			String todayAudioFolder = selectedDirectory.getAbsolutePath();
			todayAudioFolderLabel.setText(todayAudioFolder);
			settings.setTodayStudyAudioOutputDir(todayAudioFolder);
			todayAudioSavedMsgLabel.setVisible(true);
		}
	}

	private File getMPlayerDirectoryByAskingUser(Window ownerWindow) {
		DirectoryChooser directoryChooser = new DirectoryChooser();
		String dialogMessage = "Point to directory with mplayer.exe inside (like D:\\MPlayer). "
				+ "I used MPlayer Redxii-SVN-r38151-6.2.0 (i686) Compiled on 2019-08-27 02:09:02 EDT (rev. 1)";
		directoryChooser.setTitle(dialogMessage);
		// Directory with files to add to Repetition Backlog
		File dirOfMPlayer;
		dirOfMPlayer = directoryChooser.showDialog(ownerWindow);
		return dirOfMPlayer;
	}

}
