package ru.codeuniverse.audiocrammer.view;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.application.Application;
import javafx.application.HostServices;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import ru.codeuniverse.audiocrammer.model.Settings;

public class FXMain extends Application {

	public static MainViewController mainViewController;

	private static final Logger logger = LoggerFactory.getLogger(FXMain.class);

	static HostServices hostServices;

	public FXMain() {
		hostServices = getHostServices();
	}

	private static Stage primaryStage;

	public static Stage getPrimaryStage() {
		return primaryStage;
	}

	private void setPrimaryStage(Stage primaryStage) {
		this.primaryStage = primaryStage;
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		setPrimaryStage(primaryStage);

		Parent root = getMainSceneRoot();
		logger.info("root is " + root);
		Scene mainScene = new Scene(root);
		mainScene.getStylesheets()
				.add(getClass().getResource("application.css").toExternalForm());
		primaryStage.setScene(mainScene);
		primaryStage.setMaximized(false);
		setAppIcon(primaryStage);
		String windowTitle = "Audio Crammer";
		primaryStage.setTitle(windowTitle);
		primaryStage.show();
	}

	private void setAppIcon(Stage primaryStage) {
		InputStream appIconImg = getClass().getResourceAsStream(Settings.APP_ICON);
		Image appIcon = new Image(appIconImg);
		primaryStage.getIcons().add(appIcon);
	}

	private Parent getMainSceneRoot() {
		URL url = getClass().getResource("MainView.fxml");
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(url);
		BorderPane root = null;
		try {
			root = (BorderPane) loader.load();
			mainViewController = (MainViewController) loader.getController();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return root;
	}

	@Override
	public void stop() throws Exception {
		mainViewController.closeDaoConnection();
	}

	public static void main(String[] args) {
		launch(args);
	}

}
