package ru.codeuniverse.audiocrammer.utility;

import java.time.LocalDate;

public final class Utilities {
	
	private Utilities() {
		// disallow instance creation
	}
	
	/**
	 * Returns 1 if (arg < 1.0).
	 * Otherwise uses banking rounding to average-out 3.5=4.0 / 4.5=4.0 
	 * @param arg
	 * @return
	 */
	public static long roundSpecial(double arg) {
		if (arg == 0.0) {
			return 0;
		} else if (arg < 1.0) {
			return 1L;
		}
		else {
			return (long) Math.rint(arg);
		}
	}
	
	/**
	 * Retuns 1 if (arg < 1.0).
	 * Otherwise uses banking rounding to average-out 3.5=4.0 / 4.5=4.0 
	 * @param arg
	 * @return
	 */
	public static int roundSpecial(float arg) {
		if (arg < 1.0) {
			return 1;
		}
		else {
			return (int) Math.rint(arg);
		}
	}
	
	
	
	/**
	 * plusNDays("2017-03-17", 3) returns "2017-03-20"
	 * 
	 * @param ISODate
	 * @param nDays
	 * @return
	 */
	public static String plusNDays(String ISODate, int nDays) {
		LocalDate date = LocalDate.parse(ISODate);
		date = date.plusDays(nDays);
		return date.toString();
	}
	
	
	

	

}
