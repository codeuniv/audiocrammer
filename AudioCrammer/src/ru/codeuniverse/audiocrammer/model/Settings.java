package ru.codeuniverse.audiocrammer.model;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.InvalidPropertiesFormatException;
import java.util.List;
import java.util.Properties;
import java.util.prefs.BackingStoreException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * user preferences
 */
public class Settings {

	private static final String INTERNAL_DIR_NAME = "AudioCrammer";

	private static final Logger logger = LoggerFactory.getLogger(Settings.class);

	/**
	 * if directory from which files are added to backlog has files with different extentions it
	 * is OK, but only files with this extension are recognized. See List<Path>
	 * getAllFilesInDir(Path dir) method
	 */
	static final String SUPPORTED_AUDIOFILE_EXTENTION = "*.wma";

	// Basic Review Intervals:
	// 300 - ... +30 ... + 30....
	// TODO init for many years to go (say, 90 years)...
	// XXX
	public static final List<Integer> BASIC_REVIEW_INTERVAL;
	static {
		List<Integer> tmpLst = new ArrayList<>();
		tmpLst.addAll(Arrays.asList(0, 1, 3, 5, 8, 15, 25, 37, 83, 180, 230, 270, 360, 500, 650,
				850, 900, 960, 970, 980));
		BASIC_REVIEW_INTERVAL = Collections.unmodifiableList(tmpLst);
	}

	// Important Review Intervals:
	// XXX
	// TODO init for many years to go (say, 90 years)...
	//// 300 - ... +30 ... + 30....
	public static final List<Integer> IMPORTANT_REVIEW_INTERVAL;
	static {
		List<Integer> tmpLst = new ArrayList<>();
		tmpLst.addAll(Arrays.asList(0, 1, 2, 4, 7, 12, 17, 23, 30, 65, 129, 180, 230, 270, 360,
				500, 650, 850, 900, 960, 970, 980));
		IMPORTANT_REVIEW_INTERVAL = Collections.unmodifiableList(tmpLst);
	}

	// NOT Important Review Intervals:
	// TODO init for many years to go (say, 90 years)...
	// - 380 - ... +50 ...
	// XXX
	public static final List<Integer> NOT_IMPORTANT_REVIEW_INTERVAL;
	static {
		List<Integer> tmpLst = new ArrayList<>();
		tmpLst.addAll(Arrays.asList(0, 4, 7, 14, 29, 50, 100, 200, 300, 390, 550, 680, 890, 930,
				980, 990, 999));
		NOT_IMPORTANT_REVIEW_INTERVAL = Collections.unmodifiableList(tmpLst);
	}

	// DO NOT CHANGE (for already working app with accumulated data)
	// changing this negatively affects already working app with accumulated data:
	// new empty database will be created and used
	public static final String dbFileName = "audiocrammer.db";

	// SIZE IN BYTES OF 1 SECOND OF AUDIO RECORD
	// 1 second = 15,7 kB (.WMA of my Olympus WS-811, WS-812 default settings)
	public static final long SECOND_HAS_BYTES = 15700;

	// FOR .WMA ONLY, ONLY FOR YOUR STANDARD SETTINGS
	// THINK shall be configurable?
	public static final long MINUTE_HAS_BYTES = SECOND_HAS_BYTES * 60;

	/**
	 * number of records, returned by query to DB. Used by different algorithms. For example,
	 * number of AudioRecord objects retrieved from db. CHANGE IT IF USER EVER WANTS TO CHECK
	 * OUT MORE FILES (if user wants exactly 100 maximum, make it a bit more)
	 */
	public static final int NUM_RETRIEVED_RECORDS_LIMIT = 100;

	/**
	 * Default for max duration of all files to be studied by user (either "Get for Today Study"
	 * or "Check out New")
	 */
	private static final int MAX_DURATION_DEFAULT = 45;

	/**
	 * Default for max number of all files to be studied by user (either "Get for Today Study"
	 * or "Check out New")
	 */
	private static final int MAX_N_FILES_DEFAULT = 150;

	/**
	 * user can set MaxDuration for Checkout New on Settings page >= this limit
	 */
	public static final Integer MAX_DURATION_MIN_LIMIT = 25;

	/**
	 * user can set maxNFiles for Checkout New on Settings page >= this limit. Number of files
	 * user can checkout at once (at one "Chekout NEW" button press)
	 */
	public static final Integer MAX_N_FILES_MIN_LIMIT = 1;

	public static final String TOPIC_GROUP_DEFAULT = "IT";

	public static final String TOPIC_DEFAULT = "Java";

	public static final String SUB_TOPIC_DEFAULT = "";

	private static final String TODAY_STUDY_AUDIO = "TODAY_STUDY_AUDIO";

	public static final String APP_ICON = "/st.png";

	public static final String HELP_URL = "https://bitbucket.org/codeuniv/audiocrammer/wiki/edit/Home";

	private static final String SETTINGS_FILE_NAME = "settings.xml";

	// present in app folder => not first-ever app start
	private static final String FIRST_APP_START_MARKER_FILE_NAME = "FIRST_APP_START_MARKER_FILE.txt";

	private static final String INTERNAL_FOLDER_PARENT_DIRECTORY_DEFAULT;

	static {
		// Alternative to user.home: https://github.com/catnapgames/WinFoldersJava
		INTERNAL_FOLDER_PARENT_DIRECTORY_DEFAULT = System.getProperty("user.home");
	}

	public String getTopicGroupDefault() {
		readProperties();
		return props.getProperty("TopicGroupDefault", TOPIC_GROUP_DEFAULT);
	}

	public void setTopicGroupDefault(String topicGroupDefault) {
		props.setProperty("TopicGroupDefault", topicGroupDefault);
		saveProperties();
	}

	public String getTopicDefault() {
		readProperties();
		return props.getProperty("TopicDefault", TOPIC_DEFAULT);
	}

	public void setTopicDefault(String topicDefault) {
		props.setProperty("TopicDefault", topicDefault);
		saveProperties();
	}

	public String getSubTopicDefault() {
		readProperties();
		return props.getProperty("SubTopicDefault", SUB_TOPIC_DEFAULT);
	}

	public void setSubTopicDefault(String subTopicDefault) {
		props.setProperty("SubTopicDefault", subTopicDefault);
		saveProperties();
	}

	private Properties props;

	/**
	 * directory where app stores all its data
	 */
	private volatile String appDir;

	/**
	 * appDir default value until user changed any setting
	 */
	private final String appDirDefault;

	/**
	 * Max duration of all files to be studied by user (either "Get for Today Study" or "Check
	 * out New")
	 */
	private final int maxDuration;

	/**
	 * Max duration of all files to be studied by user (either "Get for Today Study" or "Check
	 * out New")
	 */
	public int getMaxDuration() {
		String maxDurationDefault = String.valueOf(MAX_DURATION_DEFAULT);
		readProperties();
		return Integer.parseInt(props.getProperty("maxDuration", maxDurationDefault));
	}

	/**
	 * Max duration of all files to be studied by user (either "Get for Today Study" or "Check
	 * out New")
	 */
	public void setMaxDuration(int maxDuration) {
		String maxDurationStr = String.valueOf(maxDuration);
		props.setProperty("maxDuration", maxDurationStr);
		saveProperties();
	}

	/**
	 * Max number of files user allowed to checkout at once by one press of either "Checkout
	 * NEW" or "Get for Today Study" btn
	 * 
	 * @param maxnFiles
	 */
	public void setMaxNFiles(int maxNFiles) {
		String maxNFilesStr = String.valueOf(maxNFiles);
		props.setProperty("maxNFiles", maxNFilesStr);
		saveProperties();
	}

	/**
	 * Max number of files user allowed to checkout at once by one press of either "Checkout
	 * NEW" or "Get for Today Study" btn
	 * 
	 * @param maxnFiles
	 */
	public int getMaxNFiles() {
		String maxNFilesDefault = String.valueOf(MAX_N_FILES_DEFAULT);
		readProperties();
		return Integer.parseInt(props.getProperty("maxNFiles", maxNFilesDefault));
	}

	/**
	 * All audiofiles to be presented to user are stored here. App copies audio files given by
	 * user into this dir.
	 */
	private volatile String trackedAudioDir;

	/**
	 * trackedAudioDir default value until user changed any setting
	 */
	private final String trackedAudioDirDefault;

	/**
	 * directory which app wipes and then writes to it audiofiles for repetition (both new and
	 * scheduled for repetition) from its trackedAudioDir.
	 */
	private volatile String todayStudyAudioOutputDir;

	/**
	 * outputDir (for today study audio) default value until user changed any setting
	 */
	private final String todayStudyAudioOutputDirDefault;

	/**
	 * if audio file is discontinued, its metadata are removed from DB and the file is moved
	 * from trackedAudioDir to discontinuedDir
	 */
	private volatile String discontinuedDir;

	/**
	 * directory inside which mplayer.exe is located (root MPlayer dir).
	 * 
	 * For example, N:\MPlayer, if we have N:\MPlayer\mplayer.exe
	 */
	private volatile String mPlayerDir;

	public String getMPlayerDir() {
		readProperties();
		return props.getProperty("MPlayerDir", "");
	}

	public void setMPlayerDir(String mPlayerDir) {
		props.setProperty("MPlayerDir", mPlayerDir);
		saveProperties();
	}

	public String getDiscontinuedDir() {
		readProperties();
		return props.getProperty("discontinuedDir", discontinuedDirDefault);
	}

	public void setDiscontinuedDir(String discontinuedDir) {
		props.setProperty("discontinuedDir", discontinuedDir);
		saveProperties();
	}

	/**
	 * discontinuedDir default value until user changed any setting *
	 */
	private final String discontinuedDirDefault;

	/**
	 * dir for audio for Cramming - default value until user changed any setting *
	 */
	private final String cramThisDirDefault;

	// method is final because called from constructor
	public final String getAppDir() {
		readProperties();
		return props.getProperty("appDir", appDirDefault);
	}

	public void setAppDir(String appDir) {
		props.setProperty("appDir", appDir);
		saveProperties();
	}

	public String getTrackedAudioDir() {
		readProperties();
		return props.getProperty("trackedAudioDir", trackedAudioDirDefault);
	}

	public void setTrackedAudioDir(String trackedAudioDir) {
		props.setProperty("trackedAudioDir", trackedAudioDir);
		saveProperties();
	}

	public String getTodayStudyAudioOutputDir() {
		readProperties();
		return props.getProperty("todayStudyAudioOutputDir", todayStudyAudioOutputDirDefault);
	}

	public void setTodayStudyAudioOutputDir(String todayStudyAudioOutputDir) {
		props.setProperty("todayStudyAudioOutputDir", todayStudyAudioOutputDir);
		saveProperties();
	}

	public String getCramThisOutputDir() {
		readProperties();
		return props.getProperty("cramThisOutputDir", cramThisDirDefault);
	}

	public void setCramThisOutputDir(String cramThisOutputDir) {
		props.setProperty("cramThisOutputDir", cramThisOutputDir);
		saveProperties();
	}

	/**
	 * Shall use Duration field (max total duration of all files) when decide how many files can
	 * be checked out (new or for review)
	 */
	public void setShallUseDuration(boolean shallUseDuration) {
		props.setProperty("shallUseDuration", String.valueOf(shallUseDuration));
		saveProperties();
	}

	/**
	 * Shall use Duration field (max total duration of all files) when decide how many files can
	 * be checked out (new or for review)
	 */
	public boolean getShallUseDuration() {
		readProperties();
		return Boolean.valueOf(props.getProperty("shallUseDuration", "false"));
	}

	/**
	 * Shall use nFiles field (max total number of all files) when decide how many files can be
	 * checked out (new or for review)
	 */
	public void setShallUseNFiles(boolean shallUseNFiles) {
		props.setProperty("ShallUseNFiles", String.valueOf(shallUseNFiles));
		saveProperties();
	}

	/**
	 * Shall use nFiles field (max total number of all files) when decide how many files can be
	 * checked out (new or for review)
	 */
	public boolean getShallUseNFiles() {
		readProperties();
		return Boolean.valueOf(props.getProperty("ShallUseNFiles", "true"));
	}

	/**
	 * Shall use all available files (regardless of settings, limiting number of files or their
	 * total duration) when deciding how many files can be checked out (normally such logic
	 * shall be applied for files-to-review only, not for checking-out NEW)
	 */
	public void setShallUseAllAvailableFiles(boolean shallUseAllAvailable) {
		props.setProperty("shallUseAllAvailable", String.valueOf(shallUseAllAvailable));
		saveProperties();
	}
	
	/**
	 * Shall use all available files (regardless of settings, limiting number of files or their
	 * total duration) when deciding how many files can be checked out (normally such logic
	 * shall be applied for files-to-review only, not for checking-out NEW)
	 */
	public boolean getShallUseAllAvailableFiles() {
		readProperties();
		return Boolean.valueOf(props.getProperty("shallUseAllAvailable", "false"));
	}

	File autoFillPersistedFile;

	public File getAutoFillPersistedFile() {
		return autoFillPersistedFile;
	}

	// parent folder within which this App creates its own directory
	private String internalFolderParentDirectory;

	public String getInternalFolderParentDirectory() {
		readProperties();
		return props.getProperty("internalFolderParentDirectory",
				INTERNAL_FOLDER_PARENT_DIRECTORY_DEFAULT);
	}

	public void setInternalFolderParentDirectory(String internalFolderParentDirectory) {
		props.setProperty("internalFolderParentDirectory", internalFolderParentDirectory);
		saveProperties();
	}

	// TODO ask appDirDefault at first start only
	// beware registry settings may exist from last start after user
	// discontinued usage long ago: ask "old settings found - delete them
	// or use them?"
	public Settings() {

		props = new Properties();

		readProperties();

		String internalFolderParentDirectory = initInternalFolderParentDirectory();

		this.appDirDefault = Paths.get(internalFolderParentDirectory, INTERNAL_DIR_NAME)
				.toString();
		this.trackedAudioDirDefault = Paths.get(appDirDefault, "TrackedAudio").toString();

		this.todayStudyAudioOutputDirDefault = Paths
				.get(INTERNAL_FOLDER_PARENT_DIRECTORY_DEFAULT, TODAY_STUDY_AUDIO).toString();

		this.cramThisDirDefault = Paths.get(appDirDefault, "Cramming").toString();
		this.discontinuedDirDefault = Paths.get(appDirDefault, "Discontinued").toString();

		maxDuration = MAX_DURATION_DEFAULT;

		String appDir = this.getAppDir();
		String autoFillPersistedFileName = "autoFillSettings.ser";
		autoFillPersistedFile = new File(appDir, autoFillPersistedFileName);

	}

	private String initInternalFolderParentDirectory() {
		logger.debug("Settings.initInternalFolderParentDirectory() entered");
		readProperties();
		String internalDir = props.getProperty("internalFolderParentDirectory", null);

		if (internalDir == null) {
			try {
				internalDir = GetExecutionPath(this).toString();
				props.setProperty("internalFolderParentDirectory", internalDir);
				saveProperties();
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				logger.info(
						"Settings.initInternalFolderParentDirectory() threw URISyntaxException");
				e.printStackTrace();
			}

		}

		return internalDir;
	}

	private void readProperties() {

		try {
			File settingsFileWithPath = getSettingsFileWithPath();
			FileInputStream fis = null;
			if (settingsFileWithPath.isFile()) {
				fis = new FileInputStream(settingsFileWithPath.toString());
				BufferedInputStream bis = new BufferedInputStream(fis);
				props.loadFromXML(bis);
				bis.close();
			}

		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			logger.info(
					"class Settings: constructor threw URISyntaxException loading properties");
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			logger.info(
					"class Settings: constructor threw FileNotFoundException loading properties");
			e.printStackTrace();
		} catch (InvalidPropertiesFormatException e) {
			// TODO Auto-generated catch block
			logger.info(
					"class Settings: constructor threw InvalidPropertiesFormatException loading properties");
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.info("class Settings: constructor threw IOException loading properties");
			e.printStackTrace();
		}

	}

	private void saveProperties() {

		try {
			File settingsFileWithPath = getSettingsFileWithPath();
			FileOutputStream fos = null;
			fos = new FileOutputStream(settingsFileWithPath.toString());
			BufferedOutputStream bos = new BufferedOutputStream(fos);
			props.storeToXML(bos, "AudioCrammer settings");
			bos.close();

		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			logger.info(
					"class Settings: constructor threw URISyntaxException saving properties");
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			logger.info(
					"class Settings: constructor threw FileNotFoundException saving properties");
			e.printStackTrace();
		} catch (InvalidPropertiesFormatException e) {
			// TODO Auto-generated catch block
			logger.info(
					"class Settings: constructor threw InvalidPropertiesFormatException saving properties");
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.info("class Settings: constructor threw IOException saving properties");
			e.printStackTrace();
		}

	}

	private File getSettingsFileWithPath() throws URISyntaxException {
		Path rootPath = GetExecutionPath(this);
		Path internalDirPath = rootPath.resolve(INTERNAL_DIR_NAME);
		File settingsFileWithPath = internalDirPath.resolve(SETTINGS_FILE_NAME).toFile();
		return settingsFileWithPath;
	}

	/**
	 * returns path/filename of the file, absence of which in app folder is a marker of
	 * first-ever app start (after the first app-start this file is created in app folder)
	 * 
	 * @return
	 * @throws URISyntaxException
	 */
	private static File getFirstAppStartMarkerFileNameWithPath() {
		// get root dir (contains this app's jar),
		// inside which this app's dir (aka internal dir with settings, etc) is located
		File jarDir = null;
		try {
			jarDir = new File(
					ClassLoader.getSystemClassLoader().getResource(".").toURI().getPath());
		} catch (URISyntaxException e) {
			logger.info(
					"Settings.getFirstAppStartMarkerFileNameWithPath() threw URI Exception getting jarDir (app root path)!");
		}
		Path appRootPath = jarDir.toPath();
		logger.info("Settings.getFirstAppStartMarkerFileNameWithPath() returned: "
				+ appRootPath.toString());

		// append app internal dir to app root dir
		Path appInternalDirPath = appRootPath.resolve(INTERNAL_DIR_NAME);
		File resultFileNameWithPath = appInternalDirPath
				.resolve(FIRST_APP_START_MARKER_FILE_NAME).toFile();
		return resultFileNameWithPath;
	}

	public void resetAllSettings() throws BackingStoreException {
		props.clear();
		saveProperties();
	}

	public static class FirstAppStart {

		/**
		 * @return true only if this is the very first time this app was launched on this
		 *         computer (under this user). Different launches under different users return
		 *         true even on the same computer
		 */
		public static boolean isFirstAppStart() {
			// Preferences prefs = Preferences.userNodeForPackage(Settings.class);
			// String firstAppStartUp = prefs.get("firstAppStartUp", null);
			// boolean isFirstAppStartUp = (null == firstAppStartUp) ? true : false;
			// if (isFirstAppStartUp) {
			// prefs.put("firstAppStartUp", "No");
			// }

			File firstAppStartMarkerFile = getFirstAppStartMarkerFileNameWithPath();

			if (!firstAppStartMarkerFile.exists()) {
				try {
					boolean fileWasMissing = firstAppStartMarkerFile.createNewFile();
					if (!fileWasMissing) {
						logger.info(
								"Settings.isFirstAppStart() - BROKEN LOGIC: Attempted to create firstAppStartMarkerFile MORE ONCE ONCE!");
					}
				} catch (IOException e) {
					logger.info(
							"Settings.isFirstAppStart() threw IOException creating firstAppStartMarkerFile");
				}
			}

			return firstAppStartMarkerFile.exists(); // tmp solution
		}

	}

	// returns path from which jar file with this class runs
	// works correctly both when run from executable jar, and from IDE
	private Path GetExecutionPath(Object obj) throws URISyntaxException {

		File jarDir = new File(
				ClassLoader.getSystemClassLoader().getResource(".").toURI().getPath());
		Path absolutePath = jarDir.toPath();
		logger.info("Settings.GetExecutionPath() returned: " + absolutePath.toString());

		return absolutePath;
	}
}
