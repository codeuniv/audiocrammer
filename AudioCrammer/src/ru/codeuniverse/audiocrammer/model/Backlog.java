package ru.codeuniverse.audiocrammer.model;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;

import ru.codeuniverse.audiocrammer.model.AudioRecord.DescendingDateAscendingTimeOrigComparator;
import ru.codeuniverse.audiocrammer.utility.Utilities;
import ru.codeuniverse.audiocrammer.view.FXMain;
import ru.codeuniverse.audiocrammer.view.MainViewController;

public class Backlog extends Tracker {

	private static final Logger logger = LoggerFactory.getLogger(Backlog.class);

	// all files found within dir to process
	private List<Path> filePaths;

	public Backlog() throws SQLException {

	}

	/**
	 * If directory contains files with different extensions, including non-audio, only files
	 * with Settings.SUPPORTED_AUDIOFILE_EXTENTION are recognized (read). Presence of other
	 * files is no problem (no exception). See glob arg help for newDirectoryStream for
	 * Settings.SUPPORTED_AUDIOFILE_EXTENTION. Normally only a single extension is required.
	 * 
	 * @param dir
	 * @return List of directory/filename.ext as Path objects.
	 * @throws IOException
	 */
	private List<Path> getAllFilesInDir(Path dir) throws IOException {
		List<Path> filesList = new ArrayList<>();
		String recognizedExtension = Settings.SUPPORTED_AUDIOFILE_EXTENTION;
		try (DirectoryStream<Path> dirStream = Files.newDirectoryStream(dir,
				recognizedExtension)) {
			for (Path path : dirStream) {
				boolean isNotDir = path.toFile().isFile();
				if (isNotDir) {
					filesList.add(path);
				}
			}
		} catch (DirectoryIteratorException ex) {
			// I/O error encounted during the iteration, the cause is an
			// IOException
			throw ex.getCause();
		}
		return filesList;
	}

	// copy single file (srcFile) to destDir
	// returns filename (my.wma for c:\dir\my.wma) of copied file.
	// If destDir has duplicate filename, copied file is appended rnd string
	// so this method returns file name with this rnd string
	private String copyFileIntoBacklog(Path srcFile, Path destDir) throws IOException {
		String resultString = "";
		try {
			Path destFile = destDir.resolve(srcFile.getFileName());
			Files.copy(srcFile, destFile, StandardCopyOption.COPY_ATTRIBUTES);
			resultString = srcFile.getFileName().toString();
		} catch (FileAlreadyExistsException e) {
			// append random string to target file name
			// and copy again
			Calendar cal = Calendar.getInstance();
			int rndNumber = cal.get(Calendar.MILLISECOND);
			String suffix = "_" + rndNumber;
			logger.info("suffix = " + suffix);
			String nameAndExtention = srcFile.getFileName().toString();
			String baseName = FilenameUtils.getBaseName(nameAndExtention);
			String extention = FilenameUtils.getExtension(nameAndExtention);
			String appendedFileNameStr = baseName + suffix + "." + extention;
			Path appendedFileName = Paths.get(appendedFileNameStr);
			Path destFile = destDir.resolve(appendedFileName);
			// FIXME can again throw FileAlreadyExistsException
			Files.copy(srcFile, destFile, StandardCopyOption.COPY_ATTRIBUTES);
			resultString = appendedFileNameStr;
		}

		return resultString;
	}

	// copies all files in srcFiles to destDir
	// XXX delete this method as not needed
	private void copyFiles(List<Path> srcFiles, Path destDir) throws IOException {
		for (Path srcFile : srcFiles) {
			try {
				Path destFile = destDir.resolve(srcFile.getFileName());
				Files.copy(srcFile, destFile, StandardCopyOption.COPY_ATTRIBUTES);
			} catch (FileAlreadyExistsException e) {
				// append random string to target file name
				// and copy again
				Calendar cal = Calendar.getInstance();
				int rndNumber = cal.get(Calendar.MILLISECOND);
				String suffix = "_" + rndNumber;
				logger.info("suffix = " + suffix);
				String nameAndExtention = srcFile.getFileName().toString();
				String baseName = FilenameUtils.getBaseName(nameAndExtention);
				String extention = FilenameUtils.getExtension(nameAndExtention);
				String appendedFileNameStr = baseName + suffix + "." + extention;
				Path appendedFileName = Paths.get(appendedFileNameStr);
				Path destFile = destDir.resolve(appendedFileName);
				// FIXME can again throw FileAlreadyExistsException
				Files.copy(srcFile, destFile, StandardCopyOption.COPY_ATTRIBUTES);
			}
		}
	}

	/**
	 * Creates AudioRecord object for a new audio file which is supposed to be then first added
	 * into Backlog (adding to Backlog and writing to DB is done by another method. DOES NOT
	 * TEST FOR FILE TYPE (EXTENSION).
	 * 
	 * @param filePath
	 * @param topics
	 * @return AudioRecord object with fields origName, fileSize and origModifiedTime
	 *         initialized from audio file given as arg.
	 */
	private AudioRecord createAudioRecordFromNewFile(Path filePath, Topics topics) {
		AudioRecord audioRecord = new AudioRecord();
		audioRecord.setTopicGroup(topics.getTopicGroup());
		audioRecord.setTopic(topics.getTopic());
		audioRecord.setSubTopic(topics.getSubTopic());
		audioRecord.fileSize = filePath.toFile().length();
		audioRecord.origName = FilenameUtils.getBaseName(filePath.getFileName().toString());
		long millisLastModified = filePath.toFile().lastModified();
		// TODO as of 4.0 will be replaced by
		// ISO_8601_EXTENDED_DATETIME_FORMAT

		String lastModifiedISO = DateFormatUtils.ISO_DATETIME_FORMAT.format(millisLastModified);
		audioRecord.origModifiedTime = lastModifiedISO;
		StringBuilder logMsg = new StringBuilder();
		logMsg.append("Created new AudioRecord for file: " + filePath.toString() + " : ");
		logMsg.append("[origName = " + audioRecord.origName + ", ");
		logMsg.append("fileSize = " + audioRecord.fileSize + ", ");
		logMsg.append("origModifiedTime = " + audioRecord.origModifiedTime + " ]");
		logger.info(logMsg.toString());

		return audioRecord;
	}

	/**
	 * Copies to this app's internal dir all new files in directory given by the user and saves
	 * these files metadata to DB.
	 * 
	 * @param dir
	 * @param topics
	 * @return number of files, successfully added to Backlog
	 * @throws Exception
	 *             - wraps SQLException (call getCause() on Exception)
	 */
	public int addFromDir(File dir, Topics topics) throws Exception {
		if (dir == null) {
			logger.debug("addFromDir(File dir) method: arg is null!");
			return 0;
		}

		this.filePaths = getAllFilesInDir(dir.toPath());

		int countAddedItems = 0;
		double totalDurationMin = 0;
		for (Path filePath : filePaths) {
			if (addNewBacklogItemToDB(filePath, topics)) {
				countAddedItems++;
				// count total length (in min.) of all files added
				totalDurationMin = totalDurationMin
						+ (filePath.toFile().length() / (double) Settings.MINUTE_HAS_BYTES);
				// XXX refactor this!!!
				MainViewController.totalDurationMinOfAddedToBacklog = Utilities
						.roundSpecial(totalDurationMin);

			}
		}

		FXMain.mainViewController.updateStatusBar();

		return countAddedItems;
	}

	/**
	 * adds to database one record (AudioRecord object), corresponding to a new audio file,
	 * added to Study Backlog.
	 * 
	 * @param filePath
	 * @param topics
	 * @return false if record about a file with same origModifiedTime and fileSize already
	 *         exists in DB (in this case this method does nothing), true otherwise.
	 * @throws Exception
	 */
	private boolean addNewBacklogItemToDB(Path filePath, Topics topics) throws Exception {

		if (hasSameFileInBacklogAlready(filePath)) {
			return false;
		}

		AudioRecord audioRecord = createAudioRecordFromNewFile(filePath, topics);
		// TODO ADJUST TOPIC GROUP!
		audioRecordDAO.callBatchTasks(new Callable<Void>() {
			@Override
			public Void call() throws Exception {
				AudioRecord createResult = audioRecordDAO.createIfNotExists(audioRecord);
				logger.info("Saved to DB AudioRecord with id = " + createResult.id
						+ " and fields: " + audioRecordDAO.objectToString(createResult));
				String trackedName = copyFileIntoBacklog(filePath, trackedAudioDir);
				createResult.trackedName = trackedName;
				audioRecordDAO.update(createResult);
				logger.info("Set trackedName for AudioRecord with id = " + createResult.id
						+ " to: " + createResult.trackedName);
				return null;
			}
		});

		return true;
	}

	// returns true if database has entry for AudioRecord
	// with same origName and same origModifiedTime.
	// Different files may differ only in origModifiedTime

	/**
	 * @param filename
	 *            with path (path/filename.ext)
	 * @return true if database has entry for AudioRecord with same origName and same
	 *         origModifiedTime. Different files may differ only in origModifiedTime
	 * @throws SQLException
	 */
	private boolean hasSameFileInBacklogAlready(Path filePath) throws SQLException {
		// check that same filename was not added to Backlog earlier
		String origName = FilenameUtils.getBaseName(filePath.getFileName().toString());
		List<AudioRecord> queryResult;
		queryResult = audioRecordDAO.queryForEq("origName", origName);
		boolean isSameName = false;
		if (queryResult.size() > 0) {
			isSameName = true;
		}
		long modified = filePath.toFile().lastModified();
		String origModifiedTime = DateFormatUtils.ISO_DATETIME_FORMAT.format(modified);
		queryResult = audioRecordDAO.queryForEq("origModifiedTime", origModifiedTime);
		boolean isSameModifiedTime = false;
		if (queryResult.size() > 0) {
			isSameModifiedTime = true;
		}

		if (isSameName && isSameModifiedTime) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @param number
	 *            of AudioRecord objects to return
	 * @return List with AudioRecord objects corresponding to audiofiles from backlog never ever
	 *         studied (new).
	 * @throws SQLException
	 * 
	 */
	private List<AudioRecord> getNewAudioRecords(int numberOfFiles) throws SQLException {
		List<AudioRecord> newRecords = new ArrayList<>();
		QueryBuilder<AudioRecord, Integer> qb = audioRecordDAO.queryBuilder();
		qb.where().isNull("chekoutDate");
		qb.orderBy("origModifiedTime", false);
		PreparedQuery<AudioRecord> pq = qb.prepare();
		newRecords = audioRecordDAO.query(pq);
		logger.info("getNewAudioRecords: List<AudioRecord> size = "
				+ newRecords.subList(0, numberOfFiles).size());
		return newRecords.subList(0, numberOfFiles);
	}

	/**
	 * gets records from Database/Backlog (respective files are already present in this app
	 * internal dir) with not set chekoutDate. AMOUNT OF RETRIEVED RECORDS IS LIMITED both by
	 * totalDurationMin argument AND INTERNALLY BY NUM_RETRIEVED_RECORDS_LIMIT (so regardless of
	 * argument, it returns not more than NUM_RETRIEVED_RECORDS_LIMIT audio files from Backlog,
	 * just number of files regardless of their duration)
	 * 
	 * @param total
	 *            duration (in mins) of all audiofiles (AudioRecord objects) to return
	 * @return List with AudioRecord objects corresponding to audiofiles from backlog never ever
	 *         studied (new). Can be empty, but never null.
	 * @throws SQLException
	 * 
	 */
	private List<AudioRecord> getNewAudioRecordsWithDuration(int totalDurationMin)
			throws SQLException {

		logger.info("getNewAudioRecordsWithDuration() called with totalDurationMin = "
				+ totalDurationMin);
		// get records with not set chekoutDate
		long num_Retieved_Records_Limit = Settings.NUM_RETRIEVED_RECORDS_LIMIT;
		logger.info("NUM_RETRIEVED_RECORDS_LIMIT = " + num_Retieved_Records_Limit);
		List<AudioRecord> newRecords = new ArrayList<>();
		QueryBuilder<AudioRecord, Integer> qb = audioRecordDAO.queryBuilder();
		qb.where().isNull("chekoutDate");
		qb.orderBy("origModifiedTime", false);
		qb.limit(num_Retieved_Records_Limit);
		PreparedQuery<AudioRecord> pq = qb.prepare();
		newRecords = audioRecordDAO.query(pq);

		// sort newRecords by orig modified time ascending within same date (earliest first)
		// dates stay sorted descending (latest first).
		// Purpose: review records for same day in chronological order,
		// review most recent records first, older records later
		Comparator<AudioRecord> comparator = new DescendingDateAscendingTimeOrigComparator();
		Collections.sort(newRecords, comparator);

		// select from newRecords records whose total duration is maxTotalSizeInBytes
		// put selected records into recordsToReturn
		long maxTotalSizeInBytes = totalDurationMin * Settings.MINUTE_HAS_BYTES;
		List<AudioRecord> recordsToReturn = new ArrayList<>();
		long byteCounter = 0;
		for (AudioRecord audioRecord : newRecords) {
			byteCounter = byteCounter + audioRecord.fileSize;
			if (byteCounter <= maxTotalSizeInBytes) {
				recordsToReturn.add(audioRecord);
			} else {
				break;
			}
		}

		logger.debug("getNewAudioRecordsWithDuration(int totalDurationMin): "
				+ "given totalDurationMin = " + totalDurationMin + " returned "
				+ recordsToReturn.size() + " AudioRecord(s): " + recordsToReturn.toString());

		return recordsToReturn;
	}

	/**
	 * Type returned by checkoutNew() method - contains totalDuration and totalCount of all New
	 * files Checked-out from Backlog
	 */
	public static class DurationAndCountOfCheckoutNew {
		private int totalDuration = 0;
		private int totalCount = 0;

		public int getTotalDuration() {
			return totalDuration;
		}

		private void setTotalDuration(int totalDuration) {
			this.totalDuration = totalDuration;
		}

		public int getTotalCount() {
			return totalCount;
		}

		private void setTotalCount(int totalCount) {
			this.totalCount = totalCount;
		}
	}

	/**
	 * <pre>
	 * 1) Gets latest (by origModifiedTime) never ever reviewed (new) files from Backlog 
	 * 2) selects latest (by modified time) files 
	 *     with total play duration not more than totalDurationMin
	 * 3) deletes all files/subfolders (if any) in OutputDir 
	 * 4) copies these files (see 1,2) from app TrackedAudioDir to OutputDir for user
	 * 5) writes to database: file(s) chekoutDate
	 * 6) writes to database: file(s) nextReviewDate and nextReviewNumber
	 * </pre>
	 * 
	 * nextReviewNumber IS INCREMENTED BY 1 AFTER THIS CALL. 
	 * THIS METHOD SHALL BE CALLED ONLY ONCE (IT DOES NOT CHECK FOR IT) !!!
	 * 
	 * @param totalDurationMin
	 * @return DurationAndCountOfCheckoutNew object, containing total duration (in mins) of
	 *         checked-out files, and the number of those files
	 * @throws Exception
	 */
	/**
	 * @param totalDurationMin
	 * @return
	 * @throws Exception
	 */
	public DurationAndCountOfCheckoutNew checkoutNew(int totalDurationMin) throws Exception {

		// checkIfNewAllowed(); shall be called first Where GUI available

		// steps 1,2 (see javadoc) : get latest with given duration
		List<AudioRecord> records = new ArrayList<>();
		records = getNewAudioRecordsWithDuration(totalDurationMin);

		// wipe OutputDir before writing to it
		wipeOutputDir();

		// steps 4,5,6 - within transaction
		// step 4: copy file(s) to OutputDir
		// steps 5,6: writes to database: files chekoutDate, nextReviewDate, nextReviewNumber
		Date today = new Date();
		// will be replaced by ISO_8601_EXTENDED_DATE_FORMAT
		String todayDateISO = DateFormatUtils.ISO_DATE_FORMAT.format(today);
		String nextReviewISODate = null;
		for (AudioRecord audioRecord : records) {
			audioRecord.chekoutDate = todayDateISO;
			// set first repetition interval for basic importance
			int nDays = Settings.BASIC_REVIEW_INTERVAL.get(1);
			nextReviewISODate = plusNDays(todayDateISO, nDays);
			audioRecord.nextReviewDate = nextReviewISODate;
			audioRecord.nextReviewNumber++;
			audioRecordDAO.callBatchTasks(new Callable<Void>() {
				@Override
				public Void call() throws Exception {
					audioRecordDAO.update(audioRecord);
					copyFromTrackedToOutputDir(audioRecord);
					return null;
				}
			});
		}

		// return total duration (in mins) of all files copied and logged to DB
		long totalSizeInBytes = 0;
		for (AudioRecord audioRecord : records) {
			totalSizeInBytes = totalSizeInBytes + audioRecord.fileSize;
		}
		double totalDurationInMin = ((double) totalSizeInBytes / Settings.MINUTE_HAS_BYTES);
		logger.debug("checkoutNew method returned totalDurationInMin: " + totalDurationInMin);

		FXMain.mainViewController.updateStatusBar();

		DurationAndCountOfCheckoutNew result = new DurationAndCountOfCheckoutNew();
		result.setTotalDuration((int) Utilities.roundSpecial(totalDurationInMin));

		int totalFilesCount = records.size();
		result.setTotalCount(totalFilesCount);

		return result;
	}

	/**
	 * @return total number of all files in db to work with (including already checked-out, with
	 *         pending review, not yet checked-out)
	 * @throws SQLException
	 */
	public int countAllFiles() throws SQLException {
		return (int) audioRecordDAO.countOf();
	}

	/**
	 * total duration (in minutes) of all files in db to work with (including already
	 * checked-out, with pending review, not yet checked-out)
	 * 
	 * @return
	 * @throws SQLException
	 */
	public long countAllFilesDuration() throws SQLException {
		double totalDuration = 0;

		for (AudioRecord audioRecord : audioRecordDAO) {
			double fileDurationInMinutes = ((double) audioRecord.getFileSize()
					/ Settings.MINUTE_HAS_BYTES);
			totalDuration = totalDuration + fileDurationInMinutes;
		}
		return Utilities.roundSpecial(totalDuration);
	}

	/**
	 * @return total number of all files not yet checked-out from db
	 * @throws SQLException
	 */
	public int countNotCheckedOut() throws SQLException {
		QueryBuilder<AudioRecord, Integer> qb = audioRecordDAO.queryBuilder();
		PreparedQuery<AudioRecord> preparedQuery;
		preparedQuery = qb.setCountOf(true).where().isNull("chekoutDate").prepare();
		int countNotCheckedOut;
		countNotCheckedOut = (int) audioRecordDAO.countOf(preparedQuery);
		return countNotCheckedOut;
	}

	/**
	 * @return total duration (in min.) of all files not yet checked-out from db
	 */
	public long countNotCheckedOutDuration() {
		double totalDuration = 0;
		double fileDurationInMinutes = 0;

		for (AudioRecord audioRecord : audioRecordDAO) {
			if (audioRecord.getChekoutDate() == null) {
				fileDurationInMinutes = ((double) audioRecord.getFileSize()
						/ Settings.MINUTE_HAS_BYTES);
				totalDuration = totalDuration + fileDurationInMinutes;
			}
		}

		return Utilities.roundSpecial(totalDuration);
	}

	public int countMustReview() throws SQLException {
		QueryBuilder<AudioRecord, Integer> qb = audioRecordDAO.queryBuilder();
		qb.where().isNotNull("chekoutDate");
		PreparedQuery<AudioRecord> preparedQuery = qb.setCountOf(true).prepare();
		int countMustReview;
		countMustReview = (int) audioRecordDAO.countOf(preparedQuery);
		return countMustReview;
	}

	public long countMustReviewDuration() {
		double totalDuration = 0;
		double fileDurationInMinutes = 0;

		for (AudioRecord audioRecord : audioRecordDAO) {
			if (audioRecord.getChekoutDate() != null) {
				fileDurationInMinutes = ((double) audioRecord.getFileSize()
						/ Settings.MINUTE_HAS_BYTES);
				totalDuration = totalDuration + fileDurationInMinutes;
			}

		}
		return Utilities.roundSpecial(totalDuration);
	}

	public Statistics getStatistics() throws SQLException {
		Statistics statistics = new Statistics();

		int countAllFiles = 0;
		int countNotCheckedOut = 0;
		int countCheckedOut = 0;
		double checkedOutTotalDuration = 0;
		double checkedOutFileDurationInMinutes = 0;
		double notCheckedOutFileDurationInMinutes = 0;
		double allFilesDuration = 0; // for countAllFilesDuration
		double fileDurationInMinutes = 0;
		double notCheckedOutTotalDuration = 0;

		for (AudioRecord audioRecord : audioRecordDAO) {
			// countAllFiles
			countAllFiles++;

			// countAllFilesDuration
			fileDurationInMinutes = (((double) audioRecord.getFileSize()
					/ Settings.MINUTE_HAS_BYTES));
			allFilesDuration = allFilesDuration + fileDurationInMinutes;

			// MustReview and NotChecked - count and duration
			if (audioRecord.getChekoutDate() != null) {
				countCheckedOut++; // countMustReview
				// countMustReviewDuration
				checkedOutFileDurationInMinutes = (((double) audioRecord.getFileSize()
						/ Settings.MINUTE_HAS_BYTES));
				checkedOutTotalDuration = checkedOutTotalDuration
						+ checkedOutFileDurationInMinutes;
			} else {
				countNotCheckedOut++; // countNotCheckedOut
				// countNotCheckedOutDuration
				notCheckedOutFileDurationInMinutes = (((double) audioRecord.getFileSize()
						/ Settings.MINUTE_HAS_BYTES));
				notCheckedOutTotalDuration = notCheckedOutTotalDuration
						+ notCheckedOutFileDurationInMinutes;
			}
		}

		statistics.setCountAllFiles(countAllFiles);
		statistics.setAllFilesDuration(Utilities.roundSpecial(allFilesDuration));
		statistics.setCountCheckedOut(countCheckedOut);
		statistics.setCheckedOutDuration(Utilities.roundSpecial(checkedOutTotalDuration));
		statistics.setCountNotCheckedOut(countNotCheckedOut);
		statistics.setNotCheckedOutDuration(Utilities.roundSpecial(notCheckedOutTotalDuration));

		// countScheduledNotReviewed (scheduled for today and before today)
		Date today = new Date();
		// will be replaced by ISO_8601_EXTENDED_DATE_FORMAT
		String todayDateISO = DateFormatUtils.ISO_DATE_FORMAT.format(today);
		List<AudioRecord> scheduledButNotReviewedAllRecords = new ArrayList<>();
		QueryBuilder<AudioRecord, Integer> qb = audioRecordDAO.queryBuilder();
		qb.where().le("nextReviewDate", todayDateISO);
		PreparedQuery<AudioRecord> pq = qb.prepare();
		scheduledButNotReviewedAllRecords = audioRecordDAO.query(pq);
		statistics.setCountScheduledNotReviewedAll(scheduledButNotReviewedAllRecords.size());
		// countScheduledNotReviewed duration (scheduled for today and before today)
		double scheduledNotReviewedAllDuration = 0;
		for (AudioRecord audioRecord : scheduledButNotReviewedAllRecords) {
			fileDurationInMinutes = ((double) audioRecord.getFileSize()
					/ Settings.MINUTE_HAS_BYTES);
			scheduledNotReviewedAllDuration = scheduledNotReviewedAllDuration
					+ fileDurationInMinutes;
		}
		statistics.setScheduledNotReviewedAllDuration(
				Utilities.roundSpecial(scheduledNotReviewedAllDuration));

		// countScheduleTodaydNotReviewed (scheduled for today only)
		List<AudioRecord> scheduledTodayButNotReviewedRecords = new ArrayList<>();
		QueryBuilder<AudioRecord, Integer> qb2 = audioRecordDAO.queryBuilder();
		qb2.where().eq("nextReviewDate", todayDateISO);
		PreparedQuery<AudioRecord> pq2 = qb2.prepare();
		scheduledTodayButNotReviewedRecords = audioRecordDAO.query(pq2);
		statistics
				.setCountScheduledTodayNotReviewed(scheduledTodayButNotReviewedRecords.size());
		// countScheduleTodaydNotReviewed duration (scheduled for today only)
		double scheduledTodayNotReviewedDuration = 0;
		for (AudioRecord audioRecord : scheduledTodayButNotReviewedRecords) {
			fileDurationInMinutes = ((double) audioRecord.getFileSize()
					/ Settings.MINUTE_HAS_BYTES);
			scheduledTodayNotReviewedDuration = scheduledTodayNotReviewedDuration
					+ fileDurationInMinutes;
		}
		statistics.setScheduledTodayNotReviewedDuration(
				Utilities.roundSpecial(scheduledTodayNotReviewedDuration));

		return statistics;
	}

}
