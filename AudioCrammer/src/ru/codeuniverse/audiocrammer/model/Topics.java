package ru.codeuniverse.audiocrammer.model;

public class Topics {

	String TopicGroup;

	String topic;

	public String getTopicGroup() {
		return TopicGroup;
	}

	public void setTopicGroup(String topicGroup) {
		topicGroup.trim();
		TopicGroup = topicGroup;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		topic.trim();
		this.topic = topic;
	}

	public String getSubTopic() {
		return subTopic;
	}

	public void setSubTopic(String subTopic) {
		subTopic.trim();
		this.subTopic = subTopic;
	}

	String subTopic;
}
