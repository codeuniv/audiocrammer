package ru.codeuniverse.audiocrammer.model;

public class Statistics {
	int countAllFiles;

	long allFilesDuration;

	int countNotCheckedOut;

	long notCheckedOutDuration;

	int countCheckedOut;

	long checkedOutDuration;

	int countScheduledNotReviewedAll;

	long scheduledNotReviewedAllDuration;

	int countScheduledTodayNotReviewed;

	long scheduledTodayNotReviewedDuration;

	public int getCountScheduledNotReviewedAll() {
		return countScheduledNotReviewedAll;
	}

	public void setCountScheduledNotReviewedAll(int scheduledNotReviewedAll) {
		countScheduledNotReviewedAll = scheduledNotReviewedAll;
	}

	public int getCountScheduledTodayNotReviewed() {
		return countScheduledTodayNotReviewed;
	}

	public void setCountScheduledTodayNotReviewed(int scheduledTodayNotReviewed) {
		countScheduledTodayNotReviewed = scheduledTodayNotReviewed;
	}

	public long getScheduledNotReviewedAllDuration() {
		return scheduledNotReviewedAllDuration;
	}

	public void setScheduledNotReviewedAllDuration(long scheduledNotReviewedAllDuration) {
		this.scheduledNotReviewedAllDuration = scheduledNotReviewedAllDuration;
	}

	public long getScheduledTodayNotReviewedDuration() {
		return scheduledTodayNotReviewedDuration;
	}

	public void setScheduledTodayNotReviewedDuration(long scheduledTodayNotReviewedDuration) {
		this.scheduledTodayNotReviewedDuration = scheduledTodayNotReviewedDuration;
	}

	public int getCountAllFiles() {
		return countAllFiles;
	}

	public void setCountAllFiles(int countAllFiles) {
		this.countAllFiles = countAllFiles;
	}

	public long getCountAllFilesDuration() {
		return allFilesDuration;
	}

	public int getCountNotCheckedOut() {
		return countNotCheckedOut;
	}

	public void setCountNotCheckedOut(int countNotCheckedOut) {
		this.countNotCheckedOut = countNotCheckedOut;
	}

	public long getCountNotCheckedOutDuration() {
		return notCheckedOutDuration;
	}

	public int getCountCheckedOut() {
		return countCheckedOut;
	}

	public void setCountCheckedOut(int countCheckedOut) {
		this.countCheckedOut = countCheckedOut;
	}

	public long getAllFilesDuration() {
		return allFilesDuration;
	}

	public void setAllFilesDuration(long allFilesDuration) {
		this.allFilesDuration = allFilesDuration;
	}

	public long getNotCheckedOutDuration() {
		return notCheckedOutDuration;
	}

	public void setNotCheckedOutDuration(long notCheckedOutDuration) {
		this.notCheckedOutDuration = notCheckedOutDuration;
	}

	public long getCheckedOutDuration() {
		return checkedOutDuration;
	}

	public void setCheckedOutDuration(long checkedOutDuration) {
		this.checkedOutDuration = checkedOutDuration;
	}

}
