package ru.codeuniverse.audiocrammer.model;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import ru.codeuniverse.audiocrammer.model.AudioRecord;
import ru.codeuniverse.audiocrammer.model.AudioRecord.ModifiedTimeAscendingComparator;
import ru.codeuniverse.audiocrammer.model.AudioRecord.ModifiedTimeDescendingComparator;

public class AudioRecordTest {

	// tests nested class AudioRecord.ModifiedTimeAscendingComparator
	@Test
	public void testModifiedTimeAscendingComparator_DifferentISOTimeStrShallSortOldestFirst()
			throws Exception {

		AudioRecord rec1 = new AudioRecord();
		AudioRecord rec2 = new AudioRecord();
		AudioRecord rec3 = new AudioRecord();
		rec2.origModifiedTime = "2017-06-16T12:35";
		rec1.origModifiedTime = "2017-06-16T12:37";
		rec3.origModifiedTime = "2017-06-16T12:30";

		List<AudioRecord> arrLst = new ArrayList<>();
		arrLst = Arrays.asList(rec1, rec2, rec3);
		ModifiedTimeAscendingComparator comparator = new ModifiedTimeAscendingComparator();
		arrLst.sort(comparator);
		Assert.assertEquals("2017-06-16T12:30", arrLst.get(0).origModifiedTime);
		System.out.println(arrLst.get(0).origModifiedTime);
		System.out.println(arrLst.get(1).origModifiedTime);
		System.out.println(arrLst.get(2).origModifiedTime);
	}

	// tests nested class AudioRecord.ModifiedTimeDescendingComparator
	@Test
	public void testModifiedTimeDescendingComparator_DifferentISOTimeStrShallSortLatestFirst()
			throws Exception {

		AudioRecord rec1 = new AudioRecord();
		AudioRecord rec2 = new AudioRecord();
		AudioRecord rec3 = new AudioRecord();
		rec2.origModifiedTime = "2017-06-16T12:35";
		rec1.origModifiedTime = "2017-06-16T12:37";
		rec3.origModifiedTime = "2017-06-16T12:30";

		List<AudioRecord> arrLst = new ArrayList<>();
		arrLst = Arrays.asList(rec1, rec2, rec3);
		ModifiedTimeDescendingComparator comparator = new ModifiedTimeDescendingComparator();
		arrLst.sort(comparator);
		Assert.assertEquals("2017-06-16T12:37", arrLst.get(0).origModifiedTime);
		System.out.println(arrLst.get(0).origModifiedTime);
		System.out.println(arrLst.get(1).origModifiedTime);
		System.out.println(arrLst.get(2).origModifiedTime);

	}

	// tests nested class AudioRecord.DescendingDateAscendingTimeOrigComparator
	@Test
	public void testCompare() throws Exception {
		List<AudioRecord> lst = new ArrayList<>();
		AudioRecord rec1 = new AudioRecord();
		AudioRecord rec2 = new AudioRecord();
		AudioRecord rec3 = new AudioRecord();
		AudioRecord rec4 = new AudioRecord();
		AudioRecord rec5 = new AudioRecord();
		rec1.origModifiedTime = "2017-06-15T14:42";
		rec2.origModifiedTime = "2017-06-15T10:11";
		// rec3.origModifiedTime = "2017-06-17T07:05";
		rec3.origModifiedTime = "2017-06-17T07:05";
		rec4.origModifiedTime = "2017-06-17T18:42";
		rec5.origModifiedTime = "2017-06-17T19:45";
		Comparator<AudioRecord> comparator = new AudioRecord.DescendingDateAscendingTimeOrigComparator();
		lst.addAll(Arrays.asList(rec1, rec2, rec3, rec4, rec5));
		Collections.sort(lst, comparator);
		System.out.println("***********************");
		System.out.println(lst);

		assertEquals("2017-06-17T07:05", lst.get(0).origModifiedTime);

	}

}
